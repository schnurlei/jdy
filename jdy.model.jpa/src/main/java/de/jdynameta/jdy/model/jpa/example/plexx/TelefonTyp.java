package de.jdynameta.jdy.model.jpa.example.plexx;

import java.util.Formatter;

public class TelefonTyp extends EnumValue {

    public static final TelefonTyp MOBIL = new TelefonTyp("Mobil", 10);
    public static final TelefonTyp PRIVAT = new TelefonTyp("Privat", 20);
    public static final TelefonTyp ARBEIT = new TelefonTyp("Arbeit", 30);

    protected TelefonTyp(String name, int intValue) {
        super(name, intValue);
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format(formatter.locale(), getStringValue());
    }
}
