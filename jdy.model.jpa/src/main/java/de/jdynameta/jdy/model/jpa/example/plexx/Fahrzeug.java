package de.jdynameta.jdy.model.jpa.example.plexx;

import java.math.BigDecimal;

/**
 * Eine Fahrzeug, das von einer Person besessen werden kann.
 */
public interface Fahrzeug  {
    /**
     * Der Hersteller des Fahrzeuges
     * @return der Hersteller
     */
    Hersteller getHersteller();
    void setHersteller(Hersteller hersteller);

    /**
     * Der Motor des Fahrzeugs
     * @return der Motor
     */
    Motor getMotor();
    void setMotor(Motor motor);

    Modellserie getModellserie();
    void setModellserie(Modellserie modellserie);

    Person getBesitzer();

    BigDecimal getMotorleistung(Leistungseinheit einheit);
}
