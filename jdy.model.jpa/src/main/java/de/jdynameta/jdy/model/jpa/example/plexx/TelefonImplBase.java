package de.jdynameta.jdy.model.jpa.example.plexx;


import org.eclipse.persistence.annotations.Convert;

import javax.persistence.*;

@MappedSuperclass
public class TelefonImplBase implements Telefon {

    @Column(name = "TELEFON_TYP", nullable = false)
    @Convert("plexx-enum-converter")
    private TelefonTyp telefonTyp;
    @Column(name = "TELEFON_NR", nullable = false, length =  10)
    private String telefonNr;
    @ManyToOne(targetEntity = PersonImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID")
    private Person personTelefonNummernBacklink;
    @Column(name="ID")
    @Id
    private Long id;

    public TelefonImplBase() {

    }

    @Override
    public String getTelefonNr() {
        return this.telefonNr;
    }

    @Override
    public void setTelefonNr(final String telefonNr) {
        this.telefonNr = telefonNr;
    }

    @Override
    public TelefonTyp getTelefonTyp() {
        return this.telefonTyp;
    }

    @Override
    public void setTelefonTyp(final TelefonTyp telefonTyp) {
        this.telefonTyp = telefonTyp;
    }
}
