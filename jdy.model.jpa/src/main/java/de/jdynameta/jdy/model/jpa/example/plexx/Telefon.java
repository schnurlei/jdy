package de.jdynameta.jdy.model.jpa.example.plexx;

/**
 * Telefonnr einer Person
 */
public interface Telefon  {
    String getTelefonNr();
    void setTelefonNr(String telefonNr);

    TelefonTyp getTelefonTyp();
    void setTelefonTyp(TelefonTyp telefonTyp);

}
