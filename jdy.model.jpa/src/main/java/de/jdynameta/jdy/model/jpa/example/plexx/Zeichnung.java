package de.jdynameta.jdy.model.jpa.example.plexx;

public interface Zeichnung {

    String getTeileNumber();
    void setTeileNumber(String partNumber);
}
