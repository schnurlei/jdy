package de.jdynameta.jdy.model.jpa.example.plexx;

import java.util.Formatter;

/**
 * Gibt die Einheit an, in der Leistungswerte angegeben werden sollen.
 */
public class Leistungseinheit extends EnumValue {
    public static final Leistungseinheit KWH = new Leistungseinheit("kWh", 10);
    public static final Leistungseinheit PS = new Leistungseinheit("PS", 20);

    protected Leistungseinheit(String name, int intValue) {
        super(name, intValue);
    }

    /**
     * Formatiert den Wert (mit Uebersetzung)
     * @param formatter der Formatter, der angewendet Wird
     * @param flags die Flags fuer den Formatter
     * @param width die Breite
     * @param precision die Praezision
     */
    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format(formatter.locale(), getStringValue());
    }
}
