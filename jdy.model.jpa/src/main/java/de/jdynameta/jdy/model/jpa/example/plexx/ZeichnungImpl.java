package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity(name = "Zeichnung")
@Table(name = "L_ZEICHNUNG")
public class ZeichnungImpl extends ZeichnungImplBase {

    @OneToMany(targetEntity = ZeichnungEintragImpl.class)
    @JoinColumn(name = "ZEICHNUNG_ID", referencedColumnName = "ID")
    private Collection<ZeichnungEintrag> zeichnungEintraege;

}
