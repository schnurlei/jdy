package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import java.math.BigDecimal;
import java.math.MathContext;

@Entity(name = "Motor")
@Table(name = "MOTOR")
public class MotorImpl extends MotorImplBase {

    @Column(name = "LOCK_VERSION")
    @Version
    private Integer lockVersion; // NOSONAR



}
