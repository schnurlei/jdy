package de.jdynameta.jdy.model.jpa.example.plexx;

import org.eclipse.persistence.annotations.Convert;


import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@MappedSuperclass
public class PersonImplBase implements Person {


    @OneToMany(targetEntity = TelefonImpl.class, mappedBy = "personTelefonNummernBacklink")
    private List<Telefon> telefonNummern;
    @Column(name = "VORNAME", nullable = false, length =  30)
    private String vorname;
    @Column(name = "ADRESSE", length =  120)
    private String adresse;
    @Column(name = "NACHNAME", nullable = false, length =  30)
    private String nachname;
    @Column(name = "ID")
    @Id
    private Long id;
    @OneToMany(targetEntity = FahrzeugImpl.class, mappedBy = "besitzer")
    private List<Fahrzeug> fahrzeuge;

    public PersonImplBase() {

    }

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getVorname() {
        return this.vorname;
    }

    @Override
    public void setVorname(final String vorname) {
        this.vorname = vorname;
    }

    @Override
    public String getAdresse() {
        return this.adresse;
    }

    @Override
    public void setAdresse(final String adresse) {
        this.adresse = adresse;
    }

    @Override
    public String getNachname() {
        return this.nachname;
    }

    @Override
    public void setNachname(final String nachname) {
        this.nachname = nachname;
    }

    @Override
    public List<Fahrzeug> getFahrzeuge() {
        return this.fahrzeuge;
    }

    @Override
    public List<Telefon> getTelefonNummern() {
        return this.telefonNummern;
    }
}
