package de.jdynameta.jdy.model.jpa.example.plexx;


import java.math.BigDecimal;

/**
 * Eine Motor gehoert zu einem Fahrzeug und hat eine bestimmte Leistungseinheit die entweder in kWh oder in PS gemeldet wird.
 */
public interface Motor  {
    /**
     * Der Hersteller des Motors
     * @return der Hersteller
     */
    Hersteller getHersteller();
    void setHersteller(Hersteller hersteller);

    /**
     * Die Bezeichnung des Motors
     * @return die Bezeichnung
     */
    String getBezeichnung();
    void setBezeichnung(String bezeichnung);

    /**
     * Gibt die Leistung eines Motors in kWh an
     * @return die Leistung in kWh
     */
    BigDecimal getLeistungInKwh();
    void setLeistungInKwh(BigDecimal leistungInKwh);

    /**
     * Liefert die Leistung des Motors in PS (berechnet aus der Leistung in kWh)
     * @return die Leistung in PS
     */
    BigDecimal getLeistungInPs();


}
