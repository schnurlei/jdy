/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jdynameta.jdy.model.jpa.example;

import de.jdynameta.base.view.DbDomainValue;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author rainer
 */
@Entity
@Table(name = "PLANT")
public class Plant implements Serializable {

	public enum PlantFamily implements DbDomainValue<String>
	{
		Iridaceae("Iridaceae", "Iridaceae")
		, Malvaceae("Malvaceae", "Malvaceae")
		, Geraniaceae("Geraniaceae", "Geraniaceae")
		, Lamiaceae("Lamiaceae", "Lamiaceae")
		, Asteraceae("Asteraceae", "Asteraceae");

		private final String	domValue;
		private final String	representation;

		private PlantFamily(String domValue, String representation)
		{
			this.domValue = domValue;
			this.representation = representation;
		}

		@Override
		public String getDbValue()
		{
			return domValue;
		}

		@Override
		public String getRepresentation()
		{
			return representation;
		}
	}

	private static final long serialVersionUID = 1L;
	@Id
    @Basic(optional = false)
    @Column(name = "BOTANICNAME")
	private String botanicName;
	@Column(name = "HEIGTHINCM")
	private Integer heigthInCm;
	@Column(name = "PLANTFAMILY")
	@Enumerated
	private PlantFamily plantFamily;
	@Column(name = "COLOR")
	private String color;

	private BigDecimal price;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastChanged;

	private String description;

	private Boolean wintergreen;

	@Lob
	@Basic(fetch= FetchType.LAZY)
	private byte[] picture;

	public Plant() {
	}

	public Plant(String botanicname) {
		this.botanicName = botanicname;
	}

	public String getBotanicName() {
		return botanicName;
	}

	public void setBotanicName(String botanicname) {
		this.botanicName = botanicname;
	}

	public Integer getHeigthInCm() {
		return heigthInCm;
	}

	public void setHeigthInCm(Integer heigthincm) {
		this.heigthInCm = heigthincm;
	}

	public PlantFamily getPlantFamily() {
		return plantFamily;
	}

	public void setPlantFamily(PlantFamily plantFamily) {
		this.plantFamily = plantFamily;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(final BigDecimal price) {
		this.price = price;
	}

	public Date getLastChanged() {
		return this.lastChanged;
	}

	public void setLastChanged(final Date lastChanged) {
		this.lastChanged = lastChanged;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Boolean getWintergreen() {
		return this.wintergreen;
	}

	public void setWintergreen(final Boolean wintergreen) {
		this.wintergreen = wintergreen;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (botanicName != null ? botanicName.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Plant)) {
			return false;
		}
		Plant other = (Plant) object;
		if ((this.botanicName == null && other.botanicName != null) || (this.botanicName != null && !this.botanicName.equals(other.botanicName))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "de.jdynameta.model.asm.jpa.Plant[ botanicname=" + botanicName + " ]";
	}

}
