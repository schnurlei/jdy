package de.jdynameta.jdy.model.jpa.example.plexx;

import org.eclipse.persistence.annotations.Convert;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.Collection;

@MappedSuperclass
public class ModellserieImplBase implements Modellserie {

    @Column(name = "BEZEICHNUNG", nullable = false, length =  100)
    private String bezeichnung;
    @Column(name = "NAME", nullable = false, length =  100)
    private String name;
    @Column(name = "STATUS", nullable = false)
    @Convert("plexx-enum-converter")
    private ModellStatus status;
    @Transient
    private Collection<Fahrzeug> fahrzeugModellserieBacklink;
    @Column(name="ID")
    @Id
    private Long id;

    public ModellserieImplBase() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    @Override
    public void setBezeichnung(final String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public ModellStatus getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(final ModellStatus status) {
        this.status = status;
    }
}
