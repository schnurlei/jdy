package de.jdynameta.jdy.model.jpa.example.plexx;

/**
 * Die konkrekte Modellserie eines Fahrzeugs, diese hat einen Namen (z.B. "S" oder "Astra") und eine eigene Bezeichnung
 */
public interface Modellserie  {

    String getName();
    void setName(String name);

     String getBezeichnung();
    void setBezeichnung(String bezeichung);

     ModellStatus getStatus();
    void setStatus(ModellStatus status);
}
