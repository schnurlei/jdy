package de.jdynameta.jdy.model.jpa.example.plexx;

import java.util.List;

/**
 * Eine Person, die Besitzer eines Fahrzeuges sein kann.
 */
public interface Person  {


    public Long getId();

    String getVorname();
    void setVorname(String vorname);

    String getNachname();
    void setNachname(String nachname);

    String getAdresse();
    void setAdresse(String adresse);

    List<Fahrzeug> getFahrzeuge();

    List<Telefon> getTelefonNummern();

}
