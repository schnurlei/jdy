package de.jdynameta.jdy.model.jpa.example.plexx;

public interface ZeichnungEintrag {

    String getKommentar();
    void setKommentar(String newValue);

}
