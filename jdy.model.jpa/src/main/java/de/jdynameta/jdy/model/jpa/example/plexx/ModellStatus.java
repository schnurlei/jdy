package de.jdynameta.jdy.model.jpa.example.plexx;

/**
 * Der ModellStatus einer Modellserie
 */
public class ModellStatus extends EnumValue {

    public static final ModellStatus AKTUELL = new ModellStatus("Aktuell", 1);
    public static final ModellStatus HISTORISCH = new ModellStatus("Historisch", 2);

    private ModellStatus(String name, int intValue) {
        super(name, intValue);
    }
}
