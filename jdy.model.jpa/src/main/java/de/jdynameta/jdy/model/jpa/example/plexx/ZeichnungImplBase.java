package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ZeichnungImplBase implements Zeichnung {

    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "TEILE_NUMBER", length =  1000, nullable = false)
    private String teileNumber;

    @Override
    public String getTeileNumber() {
        return teileNumber;
    }

    @Override
    public void setTeileNumber(String teileNumber) {
        this.teileNumber = teileNumber;
    }
}
