package de.jdynameta.jdy.model.jpa.example.plexx;


import org.eclipse.persistence.annotations.Convert;

import javax.persistence.*;

@MappedSuperclass
public abstract class FahrzeugImplBase  implements Fahrzeug {

    @Column(name = "HERSTELLER", nullable = false)
    @Convert("plexx-enum-converter")
    private Hersteller hersteller;
    @OneToOne(targetEntity = MotorImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "MOTOR_ID", nullable = false)
    private Motor motor;
    @ManyToOne(targetEntity = ModellserieImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "MODELLSERIE_ID", nullable = false)
    private Modellserie modellserie;
    @ManyToOne(targetEntity = PersonImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "BESITZER_ID")
    private Person besitzer;
    @Column(name="ID")
    @Id
    private Long id;

    public FahrzeugImplBase() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Hersteller getHersteller() {
        return this.hersteller;
    }

    @Override
    public void setHersteller(final Hersteller hersteller) {
        this.hersteller = hersteller;
    }

    @Override
    public Motor getMotor() {
        return this.motor;
    }

    @Override
    public void setMotor(final Motor motor) {
        this.motor = motor;
    }

    @Override
    public Modellserie getModellserie() {
        return this.modellserie;
    }

    @Override
    public void setModellserie(final Modellserie modellserie) {
        this.modellserie = modellserie;
    }

    @Override
    public Person getBesitzer() {
        return this.besitzer;
    }

    public void setBesitzer(final Person besitzer) {
        this.besitzer = besitzer;
    }
}
