package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ZeichnungEintragImplBase implements ZeichnungEintrag {

    @Column(name="ID")
    @Id
    private Long id;
    @Column(name = "MODULE_ID")
    private Long moduleId;
    @Column(name = "KOMMENTAR", length =  256)
    private String kommentar;

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }
}
