package de.jdynameta.jdy.model.jpa.example.plexx;

import org.eclipse.persistence.annotations.Convert;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class MotorImplBase  implements Motor {

    @Column(name = "HERSTELLER", nullable = false)
    @Convert("plexx-enum-converter")
    private Hersteller hersteller;
    @Column(name = "BEZEICHNUNG", nullable = false, length =  100)
    private String bezeichnung;
    @Column(name = "LEISTUNG_IN_KWH", nullable = false, precision = 30, scale = 10)
    private BigDecimal leistungInKwh;
    @Column(name = "LEISTUNG_IN_PS", precision = 30, scale = 10)
    private BigDecimal leistungInPs;
    @Transient
    private Fahrzeug fahrzeugMotorBacklink;
    @Column(name="ID")
    @Id
    private Long id;

    public MotorImplBase() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Hersteller getHersteller() {
        return this.hersteller;
    }

    @Override
    public void setHersteller(final Hersteller hersteller) {
        this.hersteller = hersteller;
    }

    @Override
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    @Override
    public void setBezeichnung(final String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    @Override
    public BigDecimal getLeistungInKwh() {
        return this.leistungInKwh;
    }

    @Override
    public void setLeistungInKwh(final BigDecimal leistungInKwh) {
        this.leistungInKwh = leistungInKwh;
    }

    @Override
    public BigDecimal getLeistungInPs() {
        return this.leistungInPs;
    }

    public void setLeistungInPs(final BigDecimal leistungInPs) {
        this.leistungInPs = leistungInPs;
    }
}
