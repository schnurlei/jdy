package de.jdynameta.jdy.model.jpa.example.plexx;

import java.util.Formatter;

public class Hersteller extends EnumValue {
    public static final Hersteller BMW = new Hersteller("BMW", 10);
    public static final Hersteller AUDI = new Hersteller("Audi", 20);

    protected Hersteller(String name, int intValue) {
        super(name, intValue);
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format(formatter.locale(), getStringValue());
    }
}
