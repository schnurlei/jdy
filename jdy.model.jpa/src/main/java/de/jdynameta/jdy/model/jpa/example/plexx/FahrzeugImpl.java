package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import java.math.BigDecimal;

@Entity(name = "Fahrzeug")
@Table(name = "FAHRZEUG")
public class FahrzeugImpl extends FahrzeugImplBase {

    @Column(name = "LOCK_VERSION")
    @Version
    private Integer lockVersion; // NOSONAR

    public BigDecimal getMotorleistung(Leistungseinheit einheit) {
        return null;
    }
}
