package de.jdynameta.jdy.model.jpa.example.plexx;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity(name = "Person")
@Table(name = "PERSON")
public class PersonImpl extends PersonImplBase {

    @Column(name = "LOCK_VERSION")
    @Version
    private Integer lockVersion; // NOSONAR
}
