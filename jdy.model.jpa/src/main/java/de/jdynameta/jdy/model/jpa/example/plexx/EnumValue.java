package de.jdynameta.jdy.model.jpa.example.plexx;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.*;

public abstract class EnumValue implements Serializable, Comparable<EnumValue>, Formattable {
    public static final int NO_INT_VALUE      = Integer.MIN_VALUE;
    private static final long serialVersionUID = 8781678918221114235L;
    private static final Map<Class<?>, EnumClassInfo> ENUM_CLASS_TO_INFO  = Collections.synchronizedMap(new HashMap<Class<?>, EnumClassInfo>());

    protected int intValue;
    protected transient String value;

    /**
     * Creates an instance with <code>intValue</code> set to <code>NO_INT_VALUE</code>.
     * Don't use this when you need long-term persistence (e.g. in a database) of enumeration values.
     * @param value The human-readable String representation of the enumeration value.
     */
    protected EnumValue(String value) {
        this(value, NO_INT_VALUE);
    }

    /**
     * Creates an instance with the given values.
     * @param value The human-readable String representation of the enumeration value.
     * @param intValue The (internal) integer representation of the enumeration value.
     * You should make sure that this value is unique within one enumeration class to avoid trouble.
     */
    protected EnumValue(String value, int intValue) {
        this.value = value;
        this.intValue = intValue;

        @SuppressWarnings("unchecked")
        Class<EnumValue> thisClass = (Class<EnumValue>) getClass();
        getEnumClassInfo(thisClass).addEnumValue(this);

        // register the values of the subclass for their superclasses
        @SuppressWarnings("unchecked")
        Class<EnumValue> superclass = (Class<EnumValue>) getClass().getSuperclass();
        while (superclass != null && !superclass.equals(EnumValue.class)) {
            getEnumClassInfo(superclass).addEnumValue(this);
            superclass = (Class<EnumValue>) superclass.getSuperclass();
        }
    }

    /**
     * Checks whether this enum value contains in the given list.
     */
    public final boolean in(final EnumValue... enumValues) {
        return enumValues != null && in(Arrays.asList(enumValues));
    }

    /**
     * Checks whether this enum value contains in the given collection.
     */
    public final boolean in(final Collection<EnumValue> enumValues) {
        return enumValues != null && enumValues.contains(this);
    }

    /**
     * The (internal) integer representation of the enumeration value.
     * @return the (internal) integer representation of the enumeration value.
     */
    public int getIntValue() {
        return intValue;
    }

    /**
     * The human-readable String representation of the enumeration value.
     * @return the human-readable String representation of the enumeration value.
     */
    public String getStringValue() {
        return value;
    }

    /**
     * For debugging purposes.
     * @return the class name, string and integer representation of the enumeration value.
     */
    public String toString() {
        StringBuilder ret = new StringBuilder(100);
        ret.append(getClass().getName());
        ret.append(": value=").append(value);
        ret.append(" (int=").append(intValue).append(')');
        return ret.toString();
    }

    /**
     * Two enumeration values are equal if and only if both their classes and their integer values are identical.
     * @return <code>true</code> if the above requirement is met, <code>false</code> otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof EnumValue) {
            EnumValue v = (EnumValue)obj;
            return obj.getClass().equals(this.getClass()) && v.value.equals(this.value) && v.intValue == this.intValue;
        }
        else {
            return false;
        }
    }

    /**
     * Enumeration values are compared by their integer value.
     * @return the comparison result of both values' integer values.
     */
    @Override
    public int compareTo(EnumValue other) {
        return (this.intValue < other.intValue ? -1 : (this.intValue == other.intValue ? 0 : 1));
    }

    /**
     * An enumeration value's hash code is the integer value.
     * @return the integer value of the enumeration value.
     */
    public int hashCode() {
        return this.intValue;
    }

    /**
     * Convenience method to return the first enumeration value.
     * @return { #first(Class) EnumValue.first(getClass())}
     */
    public EnumValue first() {
        return getEnumClassInfo(getClass()).getFirstEnumValue();
    }

    /**
     * Returns the next enumeration value when iterating through all
     * enumeration values of a class.
     * @return the next enumeration value in line.
     */
    public EnumValue next() {
        return getEnumClassInfo(getClass()).getNextEnumValue(this);

    }

    /**
     * Helper method to retrieve the first enumeration value of a class.
     * Useful for iterating through all values of a class.
     *  #next
     * @param enumValueClass The class of enumeration values to retrieve the first value of.
     * @return The first enumeration value of the given class.
     */
    public static <ENUM extends EnumValue> ENUM first(Class<ENUM> enumValueClass) {
        return getEnumClassInfo(enumValueClass).getFirstEnumValue();
    }

    /**
     * Helper method to retrieve a concrete enumeration value of a given class for a given integer value.
     * Useful when restoring enumeration values from a database.
     * @param enumValueClass the class of enumeration values
     * @param value the integer value of the enumeration value
     * @return the concrete enumeration value of type <code>enumValueClass</code> with the given
     * integer value or <code>null</code> when no such value is known.
     */
    public static <ENUM extends EnumValue> ENUM fromInt(Class<ENUM> enumValueClass, int value) {
        return getEnumClassInfo(enumValueClass).getEnumValue(value);
    }

    /**
     * Helper method to retrieve a concrete enumeration value of a given class for a given String value.
     * @param enumValueClass the class of enumeration values
     * @param value the String value of the enumeration value
     * @return the concrete enumeration value of type <code>enumValueClass</code> with the given
     * String value or <code>null</code> when no such value is known.
     */
    public static <ENUM extends EnumValue> ENUM fromString(Class<ENUM> enumValueClass, String value) {
        return getEnumClassInfo(enumValueClass).getEnumValue(value);
    }

    /**
     * Returns a copied List of all enumeration values contained in the enum value class.
     */
    public static <ENUM extends EnumValue> List<ENUM> getAllEnumerationValues(Class<ENUM> enumValueClass) {
        return getEnumClassInfo(enumValueClass).getEnumValueList();
    }

    /**
     * Returns an iterator for all enumeration values contained in an enum value class.
     */
    public static Iterator getEnumerationValueIterator(Class enumValueClass) {
        return getEnumClassInfo(enumValueClass).getEnumValueList().iterator();
    }

    /**
     * Writes the string value as formatted text.
     *  Formattable
     */
    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format(getStringValue(), (Object[]) null);
    }

    /**
     * Deserializes a concrete enumeration value.
     * @return the unique enumeration value constant
     */
    protected Object readResolve() throws ObjectStreamException {
        // This method has to be "protected" or "public", because it will be accessed from
        // the deserialization process of subclasses.
        return getEnumClassInfo(getClass()).getEnumValue(getIntValue());
    }

    /**
     * Gets or initializates EnumClassInfo instance for given enumValueClass
     */
    @SuppressWarnings("unchecked")
    private static <ENUM extends EnumValue> EnumClassInfo<ENUM> getEnumClassInfo(Class<ENUM> enumValueClass) {
        EnumClassInfo<ENUM> enumClassInfo = ENUM_CLASS_TO_INFO.get(enumValueClass);
        synchronized(enumValueClass) {
            if (enumClassInfo == null) {
                // double-checked locking optimization pattern
                enumClassInfo = ENUM_CLASS_TO_INFO.get(enumValueClass);
                if (enumClassInfo == null) {
                    // when list is null, we have to possibilites:
                    // 1st: the class is being initialized. when adding the first value, the list is null. that's ok.
                    // 2nd: the class has not yet been initialized and someone tries to call fromInt(). that's bad.
                    // so we've got to make sure that the class IS initialized using Class.forName().
                    enumClassInfo = new EnumClassInfo<ENUM>();
                    ENUM_CLASS_TO_INFO.put(enumValueClass, enumClassInfo);
                    try {
                        // we use class loader of given class because it can be different to the class loader
                        // of EnumValue class
                        Class.forName(enumValueClass.getName(), true, enumValueClass.getClassLoader());
                    }
                    catch (ClassNotFoundException e) {
                        throw new RuntimeException("Cannot find enum class " + enumValueClass.getName() + " with my classloader!");
                    }
                }
            }
        }
        return enumClassInfo;
    }

    /**
     * Class info for high-performance search of enum values by integer value or string
     */
    @SuppressWarnings("serial")
    private final static class EnumClassInfo<ENUM extends EnumValue> implements Serializable {
        // map stringValue to EnumValue
        private final Map<String, ENUM> strToEnumMap = new HashMap<String, ENUM>();
        // map intValue to EnumValue
        private final Map<Integer, ENUM> intToEnumMap = new HashMap<Integer, ENUM>();
        // list of intValues to store enum order
        private final List<ENUM> enumList = new LinkedList<ENUM>();

        /**
         * adds to enumvalue map and array, array will be increased if necessary
         */
        public final void addEnumValue(ENUM enumValue) {
            if (intToEnumMap.containsKey(enumValue.getIntValue())) {
                throw new IllegalStateException("Duplicate enums with int value " + enumValue.getIntValue() + '.');
            }

            intToEnumMap.put(enumValue.getIntValue(), enumValue);
            strToEnumMap.put(enumValue.getStringValue(), enumValue);

            enumList.add(enumValue);
            Collections.sort(enumList);
        }

        /**
         * uses map for enum value search
         */
        public final ENUM getEnumValue(int intValue) {
            return intToEnumMap.get(Integer.valueOf(intValue));
        }

        /**
         * uses map for enum value search
         */
        public final ENUM getEnumValue(String stringValue) {
            return strToEnumMap.get(stringValue);
        }

        /**
         * returns first element from enum values (with least int value)
         */
        public final ENUM getFirstEnumValue() {
            return enumList.get(0);
        }

        /**
         * returns next element from enum values after currectEnumValue (with next int value)
         */
        public final EnumValue getNextEnumValue(EnumValue currentEnumValue) {
            final int index = enumList.indexOf(currentEnumValue);
            if (index < enumList.size() - 1) {
                return enumList.get(index + 1);
            }
            else {
                return null;
            }
        }

        /**
         * returns list of enums
         */
        public final List<ENUM> getEnumValueList() {
            return new ArrayList<ENUM>(enumList);
        }
    }
}
