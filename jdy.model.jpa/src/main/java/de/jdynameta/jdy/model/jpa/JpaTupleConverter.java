package de.jdynameta.jdy.model.jpa;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.metainfo.ObjectReferenceAttributeInfo;
import de.jdynameta.base.metainfo.util.AttributePath;
import de.jdynameta.base.value.GenericValueObjectImpl;
import de.jdynameta.base.value.TypedValueObject;

import javax.persistence.Tuple;
import java.util.List;

public class JpaTupleConverter {

    public TypedValueObject convertTuppleToValueObject(Tuple tuple, List<AttributePath> attributePaths, ClassInfo allAttInfo) {

        final GenericValueObjectImpl valueObj = new GenericValueObjectImpl(allAttInfo);

        for (int i = 0; i < attributePaths.size(); i++) {
            Object value = tuple.get(i);
            GenericValueObjectImpl concreteObject = getOrCreateObjectForPath(valueObj, attributePaths.get(i).getPath());
            concreteObject.setValue(attributePaths.get(i).getLastInfo(), value);
        }

        return valueObj;
    }

    private GenericValueObjectImpl getOrCreateObjectForPath(GenericValueObjectImpl start, List<ObjectReferenceAttributeInfo> paths) {

        GenericValueObjectImpl current = start;
        for ( ObjectReferenceAttributeInfo path : paths) {
            if (current.hasValueFor(path)) {
                current = (GenericValueObjectImpl) current.getValue(path);
            } else {
                final GenericValueObjectImpl newObject = new GenericValueObjectImpl(path.getReferencedClass());
                current.setValue(path, newObject);
                current = newObject;
            }
        }
        return current;
    }

}
