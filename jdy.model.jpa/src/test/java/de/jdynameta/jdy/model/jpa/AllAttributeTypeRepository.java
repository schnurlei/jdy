package de.jdynameta.jdy.model.jpa;

import de.jdynameta.jdy.model.jpa.test.AllAttributeType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllAttributeTypeRepository extends CrudRepository<AllAttributeType, Long> {

}
