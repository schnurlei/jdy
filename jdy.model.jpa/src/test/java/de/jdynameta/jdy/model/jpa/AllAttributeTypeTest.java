package de.jdynameta.jdy.model.jpa;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.metainfo.ClassRepository;
import de.jdynameta.base.metainfo.ObjectReferenceAttributeInfo;
import de.jdynameta.base.metainfo.PrimitiveAttributeInfo;
import de.jdynameta.base.metainfo.filter.defaultimpl.DefaultClassInfoQuery;
import de.jdynameta.base.metainfo.filter.defaultimpl.QueryCreator;
import de.jdynameta.base.metainfo.primitive.*;
import de.jdynameta.base.metainfo.util.AttributePath;
import de.jdynameta.base.value.GenericValueObjectImpl;
import de.jdynameta.base.value.JdyPersistentException;
import de.jdynameta.base.value.TypedValueObject;
import de.jdynameta.jdy.model.jpa.test.AllAttributeType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AllAttributeTypeTest {

    private static final Logger LOG = LoggerFactory.getLogger(JpaApplication.class);

    @Autowired
    private AllAttributeTypeRepository allAttributeRepo;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void createObjectWithImageTest() throws IOException {

        final AllAttributeType createdObject = new AllAttributeType();
        createdObject.setId(1L);
        createdObject.setImageContent(readAllBytes(this.getClass().getResourceAsStream("chilli.JPG")));
        this.allAttributeRepo.save(createdObject);
    }

    @Test
    public void readMetaDataTest() {

        JpaMetamodelReader reader = new JpaMetamodelReader();
        ClassRepository repo = reader.createMetaRepository(entityManager.getEntityManager().getMetamodel(), "TestApp");
        ClassInfo allAttInfo = repo.getClassForName("AllAttributeType");

        assertThat(getPrimitivInfo(allAttInfo, "id").getType()).isInstanceOf(LongType.class);
        assertThat(getPrimitivInfo(allAttInfo, "shortData").getType()).isInstanceOf(LongType.class);
        assertThat(getPrimitivInfo(allAttInfo, "integerData").getType()).isInstanceOf(LongType.class);
        assertThat(getPrimitivInfo(allAttInfo, "longData").getType()).isInstanceOf(LongType.class);
        assertThat(getPrimitivInfo(allAttInfo, "booleanData").getType()).isInstanceOf(BooleanType.class);
        assertThat(getPrimitivInfo(allAttInfo, "clobData").getType()).isInstanceOf(TextType.class);
        assertThat(getPrimitivInfo(allAttInfo, "currencyData").getType()).isInstanceOf(CurrencyType.class);
        assertThat(getPrimitivInfo(allAttInfo, "dateData").getType()).isInstanceOf(TimeStampType.class);
        assertThat(getPrimitivInfo(allAttInfo, "floatData").getType()).isInstanceOf(FloatType.class);

        assertThat(getPrimitivInfo(allAttInfo, "doubleData").getType()).isInstanceOf(FloatType.class);
        assertThat(getPrimitivInfo(allAttInfo, "textData").getType()).isInstanceOf(TextType.class);
        assertThat(((TextType) getPrimitivInfo(allAttInfo, "textData").getType()).getTypeHint()).isNull();
        assertThat(getPrimitivInfo(allAttInfo, "timestampData").getType()).isInstanceOf(TimeStampType.class);
        assertThat(getPrimitivInfo(allAttInfo, "email").getType()).isInstanceOf(TextType.class);
        assertThat(((TextType) getPrimitivInfo(allAttInfo, "email").getType()).getTypeHint()).isEqualTo(TextType.TypeHint.EMAIL);
        assertThat(getPrimitivInfo(allAttInfo, "imageContent").getType()).isInstanceOf(BlobType.class);
    }

    private PrimitiveAttributeInfo getPrimitivInfo(ClassInfo allAttInfo, String attrName) {

        return (PrimitiveAttributeInfo) allAttInfo.getAttributeInfoForExternalName(attrName);
    }

    @Test
    public void readDataTest() throws IOException, JdyPersistentException {

        createAllAttributeType(1L);
        createAllAttributeType(2L);
        createAllAttributeType(3L);
        createAllAttributeType(4L);

        ClassRepository repo = new JpaMetamodelReader().createMetaRepository(entityManager.getEntityManager().getMetamodel(), "TestApp");
        ClassInfo allAttInfo = repo.getClassForName("AllAttributeType");

        DefaultClassInfoQuery jdyQuery = QueryCreator.start(allAttInfo)
                .greater("id", 2l)
                .select("id")
                .select("shortData")
                .select("integerData")
                .select("longData")
                .select("booleanData")
                .select("clobData")
                .select("dateData")
                .select("floatData")
                .select("doubleData")
                .select("textData")
                .select("timestampData")
                .select("email")
                .select("imageContent")
                .query();
        CriteriaQuery<Tuple> jpaQuery = new JpaFilterConverter(this.entityManager.getEntityManager()).convertWithSelect(jdyQuery);
        List<Tuple> readedObjects = this.entityManager.getEntityManager().createQuery(jpaQuery).getResultList();

        final List<TypedValueObject> wrappedEnitites = readedObjects.stream()
                .map(entity-> convertTuppleToValueObject(entity,jdyQuery.selectAttributes(), allAttInfo))
                .collect(Collectors.toList());
        assertThat(wrappedEnitites).hasSize(2);
        wrappedEnitites.sort(createIdComperator());
        TypedValueObject entity0 = wrappedEnitites.get(0);
        assertThat(entity0.getAttrValue("textData")).isEqualTo("3textData");
    }

    private Comparator<? super TypedValueObject> createIdComperator() {

        return (o1,o2) -> {
            try {
                Long id1 =  (Long) o1.getAttrValue("id");
                Long id2 =  (Long) o2.getAttrValue("id");
                return id1.compareTo(id2);
            } catch (JdyPersistentException ex) {
                throw new RuntimeException(ex);
            }
        };
    }

    private TypedValueObject convertTuppleToValueObject(Tuple tuple, List<AttributePath> attributePaths, ClassInfo allAttInfo) {

        final GenericValueObjectImpl valueObj = new GenericValueObjectImpl(allAttInfo);

        for (int i = 0; i < attributePaths.size(); i++) {
            Object value = tuple.get(i);
            GenericValueObjectImpl concreteObject = getOrCreateObjectForPath(valueObj, attributePaths.get(i).getPath());
            concreteObject.setValue(attributePaths.get(i).getLastInfo(), value);
        }

        return valueObj;
    }

    private GenericValueObjectImpl getOrCreateObjectForPath(GenericValueObjectImpl start, List<ObjectReferenceAttributeInfo> paths) {

        GenericValueObjectImpl current = start;
        for ( ObjectReferenceAttributeInfo path : paths) {
            if (current.hasValueFor(path)) {
                current = (GenericValueObjectImpl) current.getValue(path);
            } else {
                final GenericValueObjectImpl newObject = new GenericValueObjectImpl(path.getReferencedClass());
                current.setValue(path, newObject);
                current = newObject;
            }
        }
        return current;
    }

    private void createAllAttributeType(long id) throws IOException {

        final AllAttributeType createdObject = new AllAttributeType();
        createdObject.setId(id);
        createdObject.setBooleanData(true);
        createdObject.setClobData("ClobData"+id);
        createdObject.setCurrencyData(new BigDecimal(id));
        createdObject.setDateData(new Date());
        createdObject.setDoubleData((double) id);
        createdObject.setEmail(id +"@example.com");
        createdObject.setFloatData((float) id);
        createdObject.setImageContent(readAllBytes(this.getClass().getResourceAsStream("chilli.JPG")));
        createdObject.setIntegerData((int) id);
        createdObject.setLongData((long) id);
        createdObject.setShortData((short) id);
        createdObject.setTextData(id + "textData");
        createdObject.setTimestampData(new Timestamp(System.currentTimeMillis()));

        this.allAttributeRepo.save(createdObject);
    }

    private byte[] readAllBytes(InputStream in) throws IOException {

        // Java 9
        //in.readAllBytes())
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        for (;;) {
            int nread = in.read(buffer);
            if (nread <= 0) {
                break;
            }
            baos.write(buffer, 0, nread);
        }
        return baos.toByteArray();
    }
}
