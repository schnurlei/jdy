import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import JdyAceRepoEditor from '@/components/editor/JdyAceRepoEditor.vue';
import JdyI18nEditTable from "@/components/i18n/JdyI18nEditTable.vue";
import JdyHolder from "@/components/JdyHolder.vue";
import store from '../store';

Vue.use(VueRouter)

function convertNameToClassInfo (name: string) {

  let classInfo = null;
  // @ts-ignore
  if (name !== null && store !== null && store.state !== null && store.state.prototyping.repo !== null) {
    // @ts-ignore
    const repo: any = store.state.prototyping.repo;
    classInfo = repo.getClassInfo(name);
  }

  return {
    classinfo: classInfo
  };
}

function convertRouteToClassInfo (route: any) {

  return convertNameToClassInfo(route.params.classinfo);
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jdy/AceEditor', component: JdyAceRepoEditor
  },
  {
    path: '/jdy/I18n', component: JdyI18nEditTable
  },
  {
    path: '/jdy/:classinfo', component: JdyHolder, props: convertRouteToClassInfo
  }
]

const router = new VueRouter({
  routes
})

export default router
