import { convertI18nEntriesInfoI18nJson, convertRepo2I18nEntries } from '@jdynameta/jdy-i18n';
// @ts-ignore
import {i18n} from "@/i18n";
import Vue from "vue";

export const prototyping = {
    namespaced: true,
    state: {
        repo: null,
        error: null,
        jdyI18n: []
    },
    actions: {

        // @ts-ignore
        setDynamicRepo ({ commit }, jdyRepo) {

            const entries = convertRepo2I18nEntries(jdyRepo, {});
            commit('setDynamicRepo', { repo: jdyRepo, jdyI18n: entries });

            const generatedMessages = convertI18nEntriesInfoI18nJson(entries);
            // @ts-ignore
            i18n.mergeLocaleMessage('de', generatedMessages['de']);
            // @ts-ignore
            i18n.mergeLocaleMessage('en', generatedMessages['en']);
        },
    },
    mutations: {
        // @ts-ignore
        setDynamicRepo (state, repoHolder) {
            Vue.set(state, 'repo', repoHolder.repo);
            Vue.set(state, 'error', null);
            Vue.set(state, 'jdyI18n', repoHolder.jdyI18n);
        }
    }
};