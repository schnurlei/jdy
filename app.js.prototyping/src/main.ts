import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import store from './store';
import { i18n } from '@/i18n';
import Vuetify, {
  VCard,  VRating,  VToolbar, VToolbarTitle, VSwitch,  VSnackbar,  VDataTable,  VBtn,  VCheckbox,  VDialog,  VRow,   VCol,
  VBanner,  VCardText,  VCardTitle,    VForm,    VContainer,    VLayout,    VSelect,  VSpacer,  VCardActions,
  VFlex, VIcon, VTextField, VDatePicker, VCombobox, VTextarea, VImg
} from 'vuetify/lib'
import {JdyFilterPanel, JdyGeneratedPanel, JdySimpleTable} from "@jdynameta/jdy-vuetify";
import JdyAssocTable from "@/components/JdyAssocTable.vue";

Vue.config.productionTip = false
Vue.component('v-switch', VSwitch);
Vue.component('v-snackbar', VSnackbar);
Vue.component('v-snackbar', VSnackbar);
Vue.component('v-data-table', VDataTable);
Vue.component('v-toolbar', VToolbar);
Vue.component('v-toolbar-title', VToolbarTitle);
Vue.component('v-btn', VBtn);
Vue.component('v-checkbox', VCheckbox);

Vue.component('v-card', VCard);
Vue.component('v-spacer', VSpacer);
Vue.component('v-card-actions', VCardActions);
Vue.component('v-flex', VFlex);

Vue.component('v-select', VSelect);
Vue.component('v-layout', VLayout);
Vue.component('v-container', VContainer);
Vue.component('v-form', VForm);

Vue.component('v-card-text', VCardText);
Vue.component('v-card-title', VCardTitle);

Vue.component('v-dialog', VDialog);
Vue.component('v-banner', VBanner);
Vue.component('v-col', VCol);
Vue.component('v-row', VRow);
Vue.component('v-icon', VIcon);
Vue.component('v-text-field', VTextField);
Vue.component('v-date-picker', VDatePicker);

Vue.component('v-combobox', VCombobox);
Vue.component('v-textarea', VTextarea);
Vue.component('v-img', VImg);

Vue.component('jdy-simple-table', JdySimpleTable);
Vue.component('jdy-filter-panel', JdyFilterPanel);
Vue.component('jdy-generated-panel', JdyGeneratedPanel);
Vue.component('jdy-assoc-table', JdyAssocTable);



Vue.use(Vuetify);

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
