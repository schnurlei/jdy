
var JDYEXAMPLE = JDYEXAMPLE || {};
JDYEXAMPLE.createPlantShopRepository = function (repoCreateFunc) {

    let rep = repoCreateFunc('ExampleRepo');

    const referenzClass = rep.addClassInfo('ReferenzClass', null);
    referenzClass.addLongAttr("id", 0 , 100000).setIsKey(true);
    referenzClass.addTextAttr("description", 2000 , null);

    //let plantType = rep.addClassInfo('Perennial', null);
    let plantType = rep.addClassInfo('Plant', null);
    plantType.addTextAttr('botanicName', 200).setIsKey(true).setNotNull(true);
    plantType.addLongAttr('heigthInCm', 0, 5000);
    plantType.addTextAttr('plantFamily', 100).setGenerated(true);
    plantType.addTextAttr('color', 100, ['blue', 'red', 'green']);
    plantType.addBooleanAttr('wintergreen');
    plantType.addDecimalAttr('price', 0.0, 200.0, 2);
    plantType.addTimeStampAttr('lastChanged', true, true);
    plantType.addVarCharAttr('description', 1000);
    plantType.addBlobAttr('picture');
    plantType.addReference('ObjectReferenceAttr1', referenzClass);

    let orderType = rep.addClassInfo('PerennialOrder', null);
    orderType.addLongAttr('OrderNr', 0, 999999999).setIsKey(true);
    orderType.addTimeStampAttr('OrderDate', true, false).setNotNull(true);

    let orderItemType = rep.addClassInfo('PerennialOrderItem', null).setStandalone(false);
    orderItemType.addLongAttr('ItemNr', 0, 1000).setIsKey(true);
    orderItemType.addLongAttr('ItemCount', 0, 1000).setNotNull(true);
    orderItemType.addDecimalAttr('Price', 0.0000, 1000.0000, 4).setNotNull(true);
    orderItemType.addReference('Plant', plantType).setIsDependent(false).setNotNull(true);

    rep.addAssociation('Items', orderType, orderItemType, 'Order', 'Order'
        , false, false, false);


    return rep;
};

JDYEXAMPLE.createPlantShopRepository;
