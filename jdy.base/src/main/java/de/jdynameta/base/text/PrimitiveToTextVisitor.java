/**
 *
 * Copyright 2011 (C) Rainer Schneider,Roggenburg <schnurlei@googlemail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.jdynameta.base.text;

import de.jdynameta.base.metainfo.primitive.*;
import de.jdynameta.base.value.JdyPersistentException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Date;

@SuppressWarnings("serial")
public final class PrimitiveToTextVisitor implements PrimitiveTypeVisitor, Serializable
{
    private String value = "";
    private final DecimalFormat decimalFormat;

    public PrimitiveToTextVisitor()
    {
        decimalFormat = new DecimalFormat();
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        decimalFormat.setGroupingUsed(true);
    }

    private void setValue(String value)
    {
        this.value = value;
    }

    public void reset()
    {
        this.value = "";
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public void handleValue(Long aValue, LongType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            setValue(aValue.toString());
        }
    }

    @Override
    public void handleValue(Boolean aValue, BooleanType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            setValue(aValue.toString());
        }
    }

    @Override
    public void handleValue(Double aValue, FloatType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            decimalFormat.setMinimumFractionDigits(10);
            decimalFormat.setMaximumFractionDigits(10);
            setValue(decimalFormat.format(aValue.doubleValue()));
            setValue(aValue.toString());
        }
    }

    @Override
    public void handleValue(Date aValue, TimeStampType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
            if (!aType.isTimePartUsed())
            {
                dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
            } else if (!aType.isDatePartUsed())
            {
                dateFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM);

            }
            setValue(dateFormat.format(aValue));
        }
    }

    @Override
    public void handleValue(BigDecimal aValue, CurrencyType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            decimalFormat.setMinimumFractionDigits((int) aType.getScale());
            decimalFormat.setMaximumFractionDigits((int) aType.getScale());
            setValue(decimalFormat.format(aValue.doubleValue()));
        }
    }

    @Override
    public void handleValue(String aValue, TextType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            setValue(aValue);
        }
    }

    @Override
    public void handleValue(String aValue, VarCharType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            setValue(aValue);
        }
    }

    @Override
    public void handleValue(byte [] aValue, BlobType aType) throws JdyPersistentException
    {
        if (aValue != null)
        {
            final String encoded = Base64.getEncoder().encodeToString(aValue);
            setValue(encoded);
        }
    }
}
