package de.jdynameta.base.metainfo.util;

import de.jdynameta.base.metainfo.ObjectReferenceAttributeInfo;
import de.jdynameta.base.metainfo.PrimitiveAttributeInfo;

import java.util.List;

public interface AttributePath {

    PrimitiveAttributeInfo getLastInfo();

    List<ObjectReferenceAttributeInfo> getPath();
}
