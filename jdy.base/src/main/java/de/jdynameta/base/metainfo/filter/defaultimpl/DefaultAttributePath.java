package de.jdynameta.base.metainfo.filter.defaultimpl;

import de.jdynameta.base.metainfo.ObjectReferenceAttributeInfo;
import de.jdynameta.base.metainfo.PrimitiveAttributeInfo;
import de.jdynameta.base.metainfo.util.AttributePath;

import java.util.Collections;
import java.util.List;

public class DefaultAttributePath implements AttributePath {

    private PrimitiveAttributeInfo lastInfo;
    private List<ObjectReferenceAttributeInfo> path;

    public DefaultAttributePath(List<ObjectReferenceAttributeInfo> path, PrimitiveAttributeInfo lastInfo) {

        this.path = path;
        this.lastInfo = lastInfo;
    }

    public DefaultAttributePath(PrimitiveAttributeInfo lastInfo) {

        this.path = Collections.emptyList();
        this.lastInfo = lastInfo;
    }


    @Override
    public PrimitiveAttributeInfo getLastInfo() {

        return this.lastInfo;
    }

    @Override
    public List<ObjectReferenceAttributeInfo> getPath() {

        return this.path;
    }
}
