package de.jdynameta.base.test;

/**
 * Customer
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public interface Customer extends de.jdynameta.base.value.ValueObject
{
    /**
     * Get the customerId
     *

     * @return get the customerId
     */
    public String getCustomerId();

    /**
     * set the customerId
     *
     * @param aCustomerId

     */
    public void setCustomerId(String aCustomerId);

    /**
     * Get the firstName
     *

     * @return get the firstName
     */
    public String getFirstName();

    /**
     * set the firstName
     *
     * @param aFirstName

     */
    public void setFirstName(String aFirstName);

    /**
     * Get the middleName
     *

     * @return get the middleName
     */
    public String getMiddleName();

    /**
     * set the middleName
     *
     * @param aMiddleName

     */
    public void setMiddleName(String aMiddleName);

    /**
     * Get the lastName
     *

     * @return get the lastName
     */
    public String getLastName();

    /**
     * set the lastName
     *
     * @param aLastName

     */
    public void setLastName(String aLastName);

}
