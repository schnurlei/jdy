package de.jdynameta.base.test;

/**
 * Plant
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public interface Plant extends de.jdynameta.base.value.ValueObject

{

    /**
     * Get the botanicName
     *

     * @return get the botanicName
     */
    public String getBotanicName();

    /**
     * set the botanicName
     *
     * @param aBotanicName

     */
    public void setBotanicName(String aBotanicName);

    /**
     * Get the heigthInCm
     *

     * @return get the heigthInCm
     */
    public Long getHeigthInCm();

    /**
     * set the heigthInCm
     *
     * @param aHeigthInCm

     */

    public void setHeigthInCm(Long aHeigthInCm);

    /**
     * Get the plantFamily
     *

     * @return get the plantFamily
     */
    public String getPlantFamily();

    /**
     * set the plantFamily
     *
     * @param aPlantFamily

     */
    public void setPlantFamily(String aPlantFamily);

    /**
     * Get the color
     *

     * @return get the color
     */
    public String getColor();

    /**
     * set the color
     *
     * @param aColor

     */
    public void setColor(String aColor);

}
