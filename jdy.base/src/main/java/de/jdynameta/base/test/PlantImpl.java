package de.jdynameta.base.test;

/**
 * PlantImpl
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public class PlantImpl extends de.jdynameta.base.value.defaultimpl.ReflectionValueObject
        implements de.jdynameta.base.test.Plant

{
    private java.lang.String botanicName;
    private java.lang.Long heigthInCm;
    private java.lang.String plantFamily;
    private java.lang.String color;

    /**
     * Constructor
     */
    public PlantImpl()
    {
        super();
    }

    /**
     * Get the botanicName
     *

     * @return get the botanicName
     */
    @Override
    public String getBotanicName()
    {
        return botanicName;
    }

    /**
     * set the botanicName
     *

     */
    @Override
    public void setBotanicName(String aBotanicName)
    {
        botanicName = (aBotanicName != null) ? aBotanicName.trim() : null;
    }

    /**
     * Get the heigthInCm
     *

     * @return get the heigthInCm
     */
    @Override
    public Long getHeigthInCm()
    {
        return heigthInCm;
    }

    /**
     * set the heigthInCm
     *

     */
    @Override
    public void setHeigthInCm(Long aHeigthInCm)
    {
        heigthInCm = aHeigthInCm;
    }

    /**
     * Get the plantFamily
     *

     * @return get the plantFamily
     */
    @Override
    public String getPlantFamily()
    {
        return plantFamily;
    }

    /**
     * set the plantFamily
     *

     */
    @Override
    public void setPlantFamily(String aPlantFamily)
    {
        plantFamily = (aPlantFamily != null) ? aPlantFamily.trim() : null;
    }

    /**
     * Get the color
     *

     * @return get the color
     */
    @Override
    public String getColor()
    {
        return color;
    }

    /**
     * set the color
     *

     */
    @Override
    public void setColor(String aColor)
    {
        color = (aColor != null) ? aColor.trim() : null;
    }

    /* (non-Javadoc)
     *  java.lang.Object#equals(java.lang.Object
     */
    @Override
    public boolean equals(Object compareObj)
    {
        PlantImpl typeObj = (PlantImpl) compareObj;
        return typeObj != null
                && (((getBotanicName() != null
                && typeObj.getBotanicName() != null
                && this.getBotanicName().equals(typeObj.getBotanicName())))
                || (getBotanicName() == null
                && typeObj.getBotanicName() == null
                && this == typeObj));
    }
    /* (non-Javadoc)
     *  java.lang.Object#hashCode()
     */

    @Override
    public int hashCode()
    {
        return ((botanicName != null) ? botanicName.hashCode() : super.hashCode());
    }

}
