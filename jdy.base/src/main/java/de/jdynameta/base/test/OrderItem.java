package de.jdynameta.base.test;

import java.math.BigDecimal;
import java.lang.Long;

/**
 * OrderItem
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public interface OrderItem extends de.jdynameta.base.value.ValueObject

{

    /**
     * Get the itemNr
     *

     * @return get the itemNr
     */
    public Long getItemNr();

    /**
     * set the itemNr
     *
     * @param aItemNr

     */
    public void setItemNr(Long aItemNr);

    /**
     * Get the count
     *

     * @return get the count
     */
    public Long getCount();

    /**
     * set the count
     *
     * @param aCount

     */
    public void setCount(Long aCount);

    /**
     * Get the price
     *

     * @return get the price
     */
    public BigDecimal getPrice();

    /**
     * set the price
     *
     * @param aPrice

     */
    public void setPrice(BigDecimal aPrice);

    /**
     * Get the plant
     *

     * @return get the plant
     */
    public Plant getPlant();

    /**
     * set the plant
     *
     * @param aPlant

     */
    public void setPlant(Plant aPlant);

    /**
     * Get the privateAddress
     *

     * @return get the privateAddress
     */
    public Address getPrivateAddress();

    /**
     * set the privateAddress
     *
     * @param aPrivateAddress

     */
    public void setPrivateAddress(Address aPrivateAddress);

    /**
     * Get the invoiceAddress
     *

     * @return get the invoiceAddress
     */
    public Address getInvoiceAddress();

    /**
     * set the invoiceAddress
     *
     * @param aInvoiceAddress

     */
    public void setInvoiceAddress(Address aInvoiceAddress);

    /**
     * Get the plantOrder
     *

     * @return get the plantOrder
     */
    public PlantOrder getPlantOrder();

    /**
     * set the plantOrder
     *
     * @param aPlantOrder

     */
    public void setPlantOrder(PlantOrder aPlantOrder);

}
