package de.jdynameta.base.test;

/**
 * Address
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public interface Address extends de.jdynameta.base.value.ValueObject

{

    /**
     * Get the addressId
     *

     * @return get the addressId
     */
    public String getAddressId();

    /**
     * set the addressId
     *
     * @param aAddressId

#     * @param addressId
     */
    public void setAddressId(String aAddressId);

    /**
     * Get the street
     *

     * @return get the street
     */
    public String getStreet();

    /**
     * set the street
     *
     * @param aStreet

     */
    public void setStreet(String aStreet);

    /**
     * Get the zipCode
     *

     * @return get the zipCode
     */
    public String getZipCode();

    /**
     * set the zipCode
     *
     * @param aZipCode

     */
    public void setZipCode(String aZipCode);

    /**
     * Get the city
     *

     * @return get the city
     */
    public String getCity();

    /**
     * set the city
     *
     * @param aCity

     */
    public void setCity(String aCity);

}
