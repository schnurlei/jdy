package de.jdynameta.base.test;

import java.util.Date;
import de.jdynameta.base.objectlist.ObjectList;

/**
 * PlantOrder
 *
 * @author Copyright &copy;
 * @author Rainer Schneider
 * @version
 */
public interface PlantOrder extends de.jdynameta.base.value.ValueObject
{

    /**
     * Get the orderNr
     *

     * @return get the orderNr
     */
    public Long getOrderNr();

    /**
     * set the orderNr
     *
     * @param aOrderNr

     */
    public void setOrderNr(Long aOrderNr);

    /**
     * Get the orderDate
     *

     * @return get the orderDate
     */
    public Date getOrderDate();

    /**
     * set the orderDate
     *
     * @param aOrderDate

     */
    public void setOrderDate(Date aOrderDate);

    /**
     * Get all ItemsColl
     *

     * @return get the Collection ofItemsColl
     */
    public ObjectList getItemsColl();

    /**
     * Set aCollection of all ItemsColl
     *
     * @param aItemsCollColl

     */
    public void setItemsColl(ObjectList aItemsCollColl);

}
