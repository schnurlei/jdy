package de.jdynameta.metamodel.filter;


import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.metainfo.ClassRepository;
import de.jdynameta.base.metainfo.filter.ClassInfoQuery;
import de.jdynameta.base.metainfo.filter.defaultimpl.*;
import de.jdynameta.base.objectlist.DefaultObjectList;
import de.jdynameta.base.test.PlantShopRepository;
import de.jdynameta.base.value.JdyPersistentException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterCreatorTest {

    @Test
    public void testCreateAppFilter() throws JdyPersistentException {
        ClassRepository plantShop = PlantShopRepository.createPlantShopRepository();
        ClassInfo plantType = plantShop.getClassForName(PlantShopRepository.Type.Plant.name());

        DefaultClassInfoQuery query = QueryCreator.start(plantType)
                .or()
                .equal("BotanicName", "Iris")
                .and()
                .greater("HeigthInCm", 30L)
                .less("HeigthInCm", 100L)
                .end()
                .end().query();

        FilterCreator creator = new FilterCreator();
        AppQuery appQuery = creator.createAppFilter(query);
        Assert.assertEquals("Query class info ", PlantShopRepository.Type.Plant.name(), appQuery.getClassName());
        Assert.assertEquals("Query repo info ", plantShop.getRepoName(), appQuery.getRepoName());
        Assert.assertNotNull("Query repo info ", appQuery.getFilterId());

        Assert.assertEquals("first epxr is or", AppOrExpr.class, appQuery.getExpr().getClass());
        Assert.assertEquals("first epxr in or is operator  ", AppOperatorExpr.class, ((AppOrExpr) appQuery.getExpr()).getOrSubExprColl().get(0).getClass());
        AppOperatorExpr firstOperator = (AppOperatorExpr) ((AppOrExpr) appQuery.getExpr()).getOrSubExprColl().get(0);
        Assert.assertEquals("BotanicName", firstOperator.getAttrName());
        Assert.assertEquals("compare value  ", "Iris", firstOperator.getTextVal());
        Assert.assertEquals("Operator type  ", AppOperatorEqual.class, firstOperator.getOperator().getClass());

        ClassInfoQuery newQuery = creator.createMetaFilter(appQuery, plantShop);
        Assert.assertEquals("Query class info ", PlantShopRepository.Type.Plant.name(), newQuery.getResultInfo().getInternalName());
        Assert.assertEquals("first epxr is or ", DefaultOrExpression.class, newQuery.getFilterExpression().getClass());

        DefaultOperatorExpression firstMetaOperator = (DefaultOperatorExpression) ((DefaultOrExpression) newQuery.getFilterExpression()).getExpressionIterator().next();
        Assert.assertEquals("compare value  ", "Iris", firstMetaOperator.getCompareValue());
        Assert.assertEquals("Operator type  ", DefaultOperatorEqual.getEqualInstance(), firstMetaOperator.getOperator());
        Assert.assertEquals("Operator type  ", "BotanicName", firstMetaOperator.getAttributeInfo().getInternalName());

    }

    @Test
    public void testCreateAppFilterWithSelect() throws JdyPersistentException {

        ClassRepository plantShop = PlantShopRepository.createPlantShopRepository();
        ClassInfo plantType = plantShop.getClassForName(PlantShopRepository.Type.Customer.name());

        DefaultClassInfoQuery query = QueryCreator.start(plantType)
                .or()
                .equal("CustomerId", "customerA")
                .end()
                .select("CustomerId")
                .select("LastName")
                .select("PrivateAddress", "AddressId")
                .select("PrivateAddress", "City")
                .query();
        assertThat(query.selectAttributes().size()).isEqualTo(4);
        AppQuery appQuery = new FilterCreator().createAppFilter(query);
        assertThat(appQuery.getSelectAttributesColl().size()).isEqualTo(4);
        assertThat(appQuery.getSelectAttributesColl().get(0).getAttrPath()).isEqualTo("CustomerId");
        assertThat(appQuery.getSelectAttributesColl().get(1).getAttrPath()).isEqualTo("LastName");
        assertThat(appQuery.getSelectAttributesColl().get(2).getAttrPath()).isEqualTo("PrivateAddress.AddressId");
        assertThat(appQuery.getSelectAttributesColl().get(3).getAttrPath()).isEqualTo("PrivateAddress.City");
    }

    @Test
    public void createMetaQuerySelect() throws JdyPersistentException {

        ClassRepository plantShop = PlantShopRepository.createPlantShopRepository();

        AppQuery appQuery = new AppQuery();
        appQuery.setClassName(PlantShopRepository.Type.Customer.name());
        appQuery.setRepoName(plantShop.getRepoName());

        List<AppAttributePath> selectAttr = new ArrayList<>();
        selectAttr.add(createAppAttributePath(appQuery, "CustomerId"));
        selectAttr.add(createAppAttributePath(appQuery, "LastName"));
        selectAttr.add(createAppAttributePath(appQuery, "PrivateAddress.AddressId"));
        selectAttr.add(createAppAttributePath(appQuery, "PrivateAddress.City"));

        appQuery.setSelectAttributesColl(new DefaultObjectList<>(selectAttr));

        FilterCreator creator = new FilterCreator();
        ClassInfoQuery newQuery = creator.createMetaFilter(appQuery, plantShop);
        assertThat(newQuery.selectAttributes().size()).isEqualTo(4);

    }

    final AppAttributePath createAppAttributePath(AppQuery appQuery, String attrName) {

        AppAttributePath path = new AppAttributePath();
        path.setAttrPath(attrName);
        path.setAppQuery(appQuery);
        return path;

    }
}
