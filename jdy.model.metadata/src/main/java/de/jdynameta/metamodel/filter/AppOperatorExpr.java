package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.value.ClassNameCreator;

import java.math.BigDecimal;
import java.util.Date;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppOperatorExpr
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppOperatorExpr extends de.jdynameta.metamodel.filter.AppFilterExpr

{
	private static final long serialVersionUID = 1L;
	private java.lang.String attrName;
	private AppPrimitiveOperator operator;
	private java.lang.Boolean booleanVal;
	private java.math.BigDecimal decimalVal;
	private java.lang.Double floatVal;
	private java.lang.Long longVal;
	private java.lang.String textVal;
	private java.util.Date timestampVal;

	/**
	 *Constructor 
	 */
	public AppOperatorExpr ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppOperatorExpr"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppOperatorExpr (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the attrName

	 * @return get the attrName
	 */
	public String getAttrName() 
	{
		return attrName;
	}

	/**
	 * set the attrName

	 * @param aAttrName
	 */
	public void setAttrName( String aAttrName) 
	{
		attrName = (aAttrName!= null) ? aAttrName.trim() : null;
	}

	/**
	 * Get the operator

	 * @return get the operator
	 */
	public AppPrimitiveOperator getOperator() 
	{
		return operator;
	}

	/**
	 * set the operator

	 * @param aOperator
	 */
	public void setOperator( AppPrimitiveOperator aOperator) 
	{
		operator = aOperator;
	}

	/**
	 * Get the booleanVal

	 * @return get the booleanVal
	 */
	public Boolean getBooleanVal() 
	{
		return booleanVal;
	}

	/**
	 * set the booleanVal

	 * @param aBooleanVal
	 */
	public void setBooleanVal( Boolean aBooleanVal) 
	{
		booleanVal = aBooleanVal;
	}

	/**
	 * Get the decimalVal

	 * @return get the decimalVal
	 */
	public BigDecimal getDecimalVal() 
	{
		return decimalVal;
	}

	/**
	 * set the decimalVal

	 * @param aDecimalVal
	 */
	public void setDecimalVal( BigDecimal aDecimalVal) 
	{
		decimalVal = aDecimalVal;
	}

	/**
	 * Get the floatVal

	 * @return get the floatVal
	 */
	public Double getFloatVal() 
	{
		return floatVal;
	}

	/**
	 * set the floatVal

	 * @param aFloatVal
	 */
	public void setFloatVal( Double aFloatVal) 
	{
		floatVal = aFloatVal;
	}

	/**
	 * Get the longVal

	 * @return get the longVal
	 */
	public Long getLongVal() 
	{
		return longVal;
	}

	/**
	 * set the longVal

	 * @param aLongVal
	 */
	public void setLongVal( Long aLongVal) 
	{
		longVal = aLongVal;
	}

	/**
	 * Get the textVal

	 * @return get the textVal
	 */
	public String getTextVal() 
	{
		return textVal;
	}

	/**
	 * set the textVal

	 * @param aTextVal
	 */
	public void setTextVal( String aTextVal) 
	{
		textVal = (aTextVal!= null) ? aTextVal.trim() : null;
	}

	/**
	 * Get the timestampVal

	 * @return get the timestampVal
	 */
	public Date getTimestampVal() 
	{
		return timestampVal;
	}

	/**
	 * set the timestampVal

	 * @param aTimestampVal
	 */
	public void setTimestampVal( Date aTimestampVal) 
	{
		timestampVal = aTimestampVal;
	}


}
