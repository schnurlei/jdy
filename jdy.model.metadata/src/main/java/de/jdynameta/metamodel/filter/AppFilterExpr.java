package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppFilterExpr
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppFilterExpr extends de.jdynameta.base.value.defaultimpl.ReflectionChangeableValueObject

{
	private static final long serialVersionUID = 1L;
	private java.lang.Long exprId;
	private AppAndExpr appAndExpr;
	private AppOrExpr appOrExpr;

	/**
	 *Constructor 
	 */
	public AppFilterExpr ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppFilterExpr"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppFilterExpr (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the exprId

	 * @return get the exprId
	 */
	public Long getExprId() 
	{
		return exprId;
	}

	/**
	 * set the exprId

	 * @param aExprId
	 */
	public void setExprId( Long aExprId) 
	{
		exprId = aExprId;
	}

	/**
	 * Get the appAndExpr

	 * @return get the appAndExpr
	 */
	public AppAndExpr getAppAndExpr() 
	{
		return appAndExpr;
	}

	/**
	 * set the appAndExpr

	 * @param aAppAndExpr
	 */
	public void setAppAndExpr( AppAndExpr aAppAndExpr) 
	{
		appAndExpr = aAppAndExpr;
	}

	/**
	 * Get the appOrExpr

	 * @return get the appOrExpr
	 */
	public AppOrExpr getAppOrExpr() 
	{
		return appOrExpr;
	}

	/**
	 * set the appOrExpr

	 * @param aAppOrExpr
	 */
	public void setAppOrExpr( AppOrExpr aAppOrExpr) 
	{
		appOrExpr = aAppOrExpr;
	}

	/* (non-Javadoc)
	 *  java.lang.Object#equals(java.lang.Object
	 */
	@Override
	public boolean equals(Object compareObj) 
	{
		AppFilterExpr typeObj = (AppFilterExpr) compareObj;
		return typeObj != null 
				&& ( 
					( 
					(getExprId() != null
					&& typeObj.getExprId() != null
					&& this.getExprId().equals( typeObj.getExprId()) )
					)
					|| ( getExprId() == null
					&& typeObj.getExprId() == null
					&& this == typeObj )
				);
	}
	/* (non-Javadoc)
	 *  java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		return 				 (( exprId != null) ? exprId.hashCode() : super.hashCode())		;
	}

}
