package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppOperatorGreater
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppOperatorGreater extends de.jdynameta.metamodel.filter.AppPrimitiveOperator

{
	private static final long serialVersionUID = 1L;
	private java.lang.Boolean isAlsoEqual;

	/**
	 *Constructor 
	 */
	public AppOperatorGreater ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppOperatorGreater"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppOperatorGreater (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the isAlsoEqual

	 * @return get the isAlsoEqual
	 */
	public Boolean getIsAlsoEqual() 
	{
		return isAlsoEqual;
	}

	/**
	 * set the isAlsoEqual

	 * @param aIsAlsoEqual
	 */
	public void setIsAlsoEqual( Boolean aIsAlsoEqual) 
	{
		isAlsoEqual = aIsAlsoEqual;
	}


}
