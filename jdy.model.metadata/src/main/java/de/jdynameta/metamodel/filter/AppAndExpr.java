package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.objectlist.DefaultObjectList;
import de.jdynameta.base.objectlist.ObjectList;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppAndExpr
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppAndExpr extends de.jdynameta.metamodel.filter.AppFilterExpr

{
	private static final long serialVersionUID = 1L;
	private ObjectList andSubExprColl = new DefaultObjectList();
	private java.lang.String exprName;

	/**
	 *Constructor 
	 */
	public AppAndExpr ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppAndExpr"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppAndExpr (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the exprName

	 * @return get the exprName
	 */
	public String getExprName() 
	{
		return exprName;
	}

	/**
	 * set the exprName

	 * @param aExprName
	 */
	public void setExprName( String aExprName) 
	{
		exprName = (aExprName!= null) ? aExprName.trim() : null;
	}

	/**
	 * Get all  AppFilterExpr
	 *
	 * @return get the Collection ofAppFilterExpr
	 */
	public de.jdynameta.base.objectlist.ObjectList getAndSubExprColl() 
	{
		return andSubExprColl;
	}

	/**
	 * Set aCollection of all AppFilterExpr
	 *
	 * @param aAppFilterExprColl
	 */
	public void setAndSubExprColl( ObjectList aAppFilterExprColl) 
	{
		andSubExprColl =  aAppFilterExprColl ;
	}


}
