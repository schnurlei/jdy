package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppAttributePath
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppAttributePath extends de.jdynameta.base.value.defaultimpl.ReflectionChangeableValueObject

{
	private static final long serialVersionUID = 1L;
	private Long pathId;
	private String attrPath;
	private AppQuery appQuery;

	/**
	 *Constructor 
	 */
	public AppAttributePath ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppAttributePath"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppAttributePath (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the pathId
	 * @return get the pathId
	 */
	public Long getPathId() 
	{
		return pathId;
	}

	/**
	 * set the pathId
	 * @param aPathId
	 */
	public void setPathId( Long aPathId) 
	{
		pathId = aPathId;
	}

	/**
	 * Get the attrName
	 * @generated
	 * @return get the attrName
	 */
	public String getAttrPath()
	{
		return attrPath;
	}

	/**
	 * set the attrName
	 * @param anAttrPath
	 */
	public void setAttrPath( String anAttrPath)
	{
		attrPath = (anAttrPath!= null) ? anAttrPath.trim() : null;
	}

	/**
	 * Get the appQuery
	 * @return get the appQuery
	 */
	public AppQuery getAppQuery()
	{
		return appQuery;
	}

	/**
	 * set the appQuery
	 * @param aAppQuery
	 */
	public void setAppQuery( AppQuery aAppQuery) 
	{
		appQuery = aAppQuery;
	}

	/* (non-Javadoc)
	 *  java.lang.Object#equals(java.lang.Object
	 */
	@Override
	public boolean equals(Object compareObj) 
	{
		AppAttributePath typeObj = (AppAttributePath) compareObj;
		return typeObj != null 
				&& ( 
					( 
					(getPathId() != null
					&& typeObj.getPathId() != null
					&& this.getPathId().equals( typeObj.getPathId()) )
					)
					|| ( getPathId() == null
					&& typeObj.getPathId() == null
					&& this == typeObj )
				);
	}
	/* (non-Javadoc)
	 *  java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		return 				 (( pathId != null) ? pathId.hashCode() : super.hashCode())		;
	}

}
