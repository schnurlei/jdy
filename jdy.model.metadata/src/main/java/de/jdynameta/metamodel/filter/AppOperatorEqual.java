package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppOperatorEqual
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppOperatorEqual extends de.jdynameta.metamodel.filter.AppPrimitiveOperator

{
	private static final long serialVersionUID = 1L;
	private java.lang.Boolean isNotEqual;

	/**
	 *Constructor 
	 */
	public AppOperatorEqual ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppOperatorEqual"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppOperatorEqual (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the isNotEqual

	 * @return get the isNotEqual
	 */
	public Boolean getIsNotEqual() 
	{
		return isNotEqual;
	}

	/**
	 * set the isNotEqual

	 * @param aIsNotEqual
	 */
	public void setIsNotEqual( Boolean aIsNotEqual) 
	{
		isNotEqual = aIsNotEqual;
	}


}
