package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.objectlist.DefaultObjectList;
import de.jdynameta.base.objectlist.ObjectList;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppOrExpr
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppOrExpr extends de.jdynameta.metamodel.filter.AppFilterExpr

{
	private static final long serialVersionUID = 1L;
	private ObjectList orSubExprColl = new DefaultObjectList();
	private java.lang.String exprName;

	/**
	 *Constructor 
	 */
	public AppOrExpr ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppOrExpr"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppOrExpr (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the exprName

	 * @return get the exprName
	 */
	public String getExprName() 
	{
		return exprName;
	}

	/**
	 * set the exprName

	 * @param aExprName
	 */
	public void setExprName( String aExprName) 
	{
		exprName = (aExprName!= null) ? aExprName.trim() : null;
	}

	/**
	 * Get all  AppFilterExpr
	 *
	 * @return get the Collection ofAppFilterExpr
	 */
	public de.jdynameta.base.objectlist.ObjectList getOrSubExprColl() 
	{
		return orSubExprColl;
	}

	/**
	 * Set aCollection of all AppFilterExpr
	 *
	 * @param aAppFilterExprColl
	 */
	public void setOrSubExprColl( ObjectList aAppFilterExprColl) 
	{
		orSubExprColl =  aAppFilterExprColl ;
	}


}
