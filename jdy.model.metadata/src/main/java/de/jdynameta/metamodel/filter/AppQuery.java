package de.jdynameta.metamodel.filter;

import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.objectlist.DefaultObjectList;
import de.jdynameta.base.objectlist.ObjectList;
import de.jdynameta.base.value.ClassNameCreator;

import static de.jdynameta.metamodel.filter.FilterRepository.NAME_CREATOR;

/**
 * AppQuery
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class AppQuery extends de.jdynameta.base.value.defaultimpl.ReflectionChangeableValueObject

{
	private static final long serialVersionUID = 1L;
	private ObjectList<AppAttributePath> selectAttributesColl = new DefaultObjectList<>();
	private java.lang.Long filterId;
	private java.lang.String repoName;
	private java.lang.String className;
	private AppFilterExpr expr;

	/**
	 *Constructor 
	 */
	public AppQuery ()
	{
		super(FilterRepository.getSingleton().getInfoForType("AppQuery"), NAME_CREATOR);
	}
	/**
	 *Constructor for subclasses
	 */
	public AppQuery (ClassInfo infoForType, ClassNameCreator aNameCreator)
	{
		super(infoForType, aNameCreator);
	}

	/**
	 * Get the filterId

	 * @return get the filterId
	 */
	public Long getFilterId() 
	{
		return filterId;
	}

	/**
	 * set the filterId

	 * @param aFilterId
	 */
	public void setFilterId( Long aFilterId) 
	{
		filterId = aFilterId;
	}

	/**
	 * Get the repoName

	 * @return get the aRepoName
	 */
	public String getRepoName() 
	{
		return repoName;
	}

	/**
	 * set the repoName

	 * @param aRepoName
	 */
	public void setRepoName( String aRepoName) 
	{
		repoName = (aRepoName!= null) ? aRepoName.trim() : null;
	}

	/**
	 * Get the className

	 * @return get the className
	 */
	public String getClassName() 
	{
		return className;
	}

	/**
	 * set the className

	 * @param aClassName
	 */
	public void setClassName( String aClassName) 
	{
		className = (aClassName!= null) ? aClassName.trim() : null;
	}

	/**
	 * Get the expr

	 * @return get the expr
	 */
	public AppFilterExpr getExpr() 
	{
		return expr;
	}

	/**
	 * set the expr

	 * @param aExpr
	 */
	public void setExpr( AppFilterExpr aExpr) 
	{
		expr = aExpr;
	}

	/**
	 * Get all  AppAttributePath
	 *
	 * @return get the Collection ofAppAttributePath
	 */
	public de.jdynameta.base.objectlist.ObjectList<AppAttributePath> getSelectAttributesColl()
	{
		return selectAttributesColl;
	}

	/**
	 * Set aCollection of all AppAttributePath
	 *
	 * @param aAppAttributePathColl
	 */
	public void setSelectAttributesColl( ObjectList<AppAttributePath> aAppAttributePathColl)
	{
		selectAttributesColl =  aAppAttributePathColl;
	}

	/* (non-Javadoc)
	 *  java.lang.Object#equals(java.lang.Object
	 */
	@Override
	public boolean equals(Object compareObj) 
	{
		AppQuery typeObj = (AppQuery) compareObj;
		return typeObj != null 
				&& ( 
					( 
					(getFilterId() != null
					&& typeObj.getFilterId() != null
					&& this.getFilterId().equals( typeObj.getFilterId()) )
					)
					|| ( getFilterId() == null
					&& typeObj.getFilterId() == null
					&& this == typeObj )
				);
	}
	/* (non-Javadoc)
	 *  java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		return 				 (( filterId != null) ? filterId.hashCode() : super.hashCode())		;
	}

}
