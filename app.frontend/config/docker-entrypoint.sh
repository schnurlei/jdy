#!/usr/bin/env sh
set -eu
envsubst '${REDIRECT_HOST}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
exec "$@"