// vue.config.js
module.exports = {
    configureWebpack: config => {
        console.log("################");
        console.log(process.env.VUE_APP_DEV_SERVER_PROXY);
        if (process.env.VUE_APP_DEV_SERVER_PROXY) {
            config.devServer = {
                proxy: process.env.VUE_APP_DEV_SERVER_PROXY
            };
        }
    },
    publicPath: process.env.VUE_APP_PUBLIC_PATH,
    transpileDependencies: [
        'vuetify'
    ]
};
