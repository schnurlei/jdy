import '@babel/polyfill';
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import JdyHolder from './components/JdyHolder.vue';
import JdyTable from './components/JdyTable.vue';
import JdyPanel from './components/JdyPanel.vue';

import router from './router';
import './registerServiceWorker';
import JdyDetailTableHolder from '@/components/JdyDetailTableHolder.vue';
import JdyFilterPanel from '@/components/filter/JdyFilterPanel.vue';
import JdyI18nStringField from '@/components/i18n/JdyI18nStringField.vue';
import store from './js/app/store';
import { i18n } from '@/i18n';
import JdyPerennialExampleEdit from '@/components/JdyPerennialExampleEdit.vue';
import JdySliderField from '@/components/JdySliderField.vue';
import Vuetify, { VCard, VToolbar, VToolbarTitle, VSwitch, VSnackbar, VDataTable, VBtn, VCheckbox, VDialog, VRow, VCol,
    VBanner, VCardText, VCardTitle, VForm, VContainer, VLayout, VSelect, VSpacer, VCardActions,
    VFlex, VIcon, VTextField, VDatePicker, VCombobox, VTextarea, VImg
} from 'vuetify/lib';

Vue.config.productionTip = false;
Vue.component('jdy-holder', JdyHolder);
Vue.component('jdy-detail-table', JdyDetailTableHolder);
Vue.component('jdy-table', JdyTable);
Vue.component('jdy-panel', JdyPanel);
Vue.component('jdy-slider', JdySliderField);
Vue.component('jdy-filter', JdyFilterPanel);
Vue.component('jdy-i18n', JdyI18nStringField);

Vue.component('jdy-perennial-edit', JdyPerennialExampleEdit);

Vue.component('v-switch', VSwitch);
Vue.component('v-snackbar', VSnackbar);
Vue.component('v-snackbar', VSnackbar);
Vue.component('v-data-table', VDataTable);
Vue.component('v-toolbar', VToolbar);
Vue.component('v-toolbar-title', VToolbarTitle);
Vue.component('v-btn', VBtn);
Vue.component('v-checkbox', VCheckbox);

Vue.component('v-card', VCard);
Vue.component('v-spacer', VSpacer);
Vue.component('v-card-actions', VCardActions);
Vue.component('v-flex', VFlex);

Vue.component('v-select', VSelect);
Vue.component('v-layout', VLayout);
Vue.component('v-container', VContainer);
Vue.component('v-form', VForm);

Vue.component('v-card-text', VCardText);
Vue.component('v-card-title', VCardTitle);

Vue.component('v-dialog', VDialog);
Vue.component('v-banner', VBanner);
Vue.component('v-col', VCol);
Vue.component('v-row', VRow);
Vue.component('v-icon', VIcon);
Vue.component('v-text-field', VTextField);
Vue.component('v-date-picker', VDatePicker);

Vue.component('v-combobox', VCombobox);
Vue.component('v-textarea', VTextarea);
Vue.component('v-img', VImg);

Vue.use(Vuetify);
Vue.use(VueI18n);

export default new Vuetify({
    icons: {
        iconfont: 'mdi'
    },
    lang: {
        t: (key, ...params) => i18n.t(key, params)
    }
});

Vue.config.productionTip = false;

new Vue({
    store,
    created () {
        store.dispatch('fetchJpaRepository');
    },
    router,
    // @ts-ignore
    vuetify,
    i18n,
    render: h => h(App)
}).$mount('#app');
