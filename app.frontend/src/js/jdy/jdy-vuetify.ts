export function convertToColumns (aClassInfo, $i18n) {
    const allColumns: any[] = [];
    aClassInfo.forEachAttr(attrInfo => {
        if (attrInfo.isPrimitive()) {
            const newCol = attrInfo.getType().handlePrimitiveKey(primitiveTypeToColumnHandler(attrInfo));
            if (newCol) {
                newCol.text = $i18n.t(attrInfo.i18n);
                allColumns.push(newCol);
            }
        }
    });
    return allColumns;
}

function primitiveTypeToColumnHandler (attrInfo) {
    return {
        handleBoolean: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName(),
                left: true
            };
        },

        handleDecimal: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName()
            };
        },

        handleTimeStamp: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName()
            };
        },

        handleFloat: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName()
            };
        },

        handleLong: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName()
            };
        },

        handleText: function (aType) {
            return {
                text: attrInfo.getInternalName(),
                align: 'left',
                value: attrInfo.getInternalName(),
                left: true
            };
        },

        handleVarChar: function (aType) {
            return null;
        },

        handleBlob: function (aType) {
            return null;
        }
    };
};
