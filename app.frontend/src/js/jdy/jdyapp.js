/*
 * jdynameta,
 * Copyright (c)2013 Rainer Schneider, Roggenburg.
 * Distributed under Apache 2.0 license
 * http://jdynameta.de
 */

const JDY = JDY || {};

JDY.app = {};

JDY.app.getRepositoryHandlers = function (reader) {

    const appRep = JDY.meta.createAppRepository();

    function loadTypeInfoFunc (aRepoName, aClassName, callback) {
        const repFilter = new JDY.base.QueryCreator(
            appRep.getClassInfo('AppRepository')).equal('applicationName', aRepoName).query();
        let newRepository;

        reader.loadValuesFromDb(repFilter, function (resultRepos) {

            JDY.meta.convertAppRepositoryToRepository(resultRepos[0], function (newRepository) {
                callback(newRepository.getClassInfo(aClassName));
            });
        });
    };

    function loadAllReposFunc (callBack) {

        const allRepoFilter = new JDY.base.QueryCreator(appRep.getClassInfo('AppRepository')).query();
        reader.loadValuesFromDb(allRepoFilter, callBack);
    };

    return {
        handleAllRepos: loadAllReposFunc,
        loadTypeInfo: loadTypeInfoFunc
    };
};
