import { JdyNamedJsonFilter } from '@/js/jdy/jdy-view';

export const mutations = {

    setDynamicRepo (state, repoHolder) {
        state.dynamic = { repo: repoHolder.repo,
            error: null,
            jdyI18n: repoHolder.jdyI18n
        };
    },

    setJpaRepo (state, repoHolder) {

        state.jpa = { repo: repoHolder.repo,
            error: null,
            jdyI18n: repoHolder.jdyI18n
        };
    },

    setJpaError (state, error) {

        state.jpa = { repo: null,
            error: error,
            jdyI18n: []
        };
    },

    addFilter (state, filterToAdd: JdyNamedJsonFilter) {

        let entityFilters = state.jdyFilters.allFilters[filterToAdd.entity];
        if (!entityFilters) {
            entityFilters = {};
            state.jdyFilters.allFilters[filterToAdd.entity] = entityFilters;
        }
        entityFilters[filterToAdd.name] = filterToAdd.expressionJson;
    }
};
