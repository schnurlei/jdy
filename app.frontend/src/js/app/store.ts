import Vue from 'vue';
import Vuex from 'vuex';
import { mutations } from './mutations';
import actions from './actions';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dynamic: { // repository create dynamically from code editor
            repo: null,
            error: null,
            jdyI18n: []
        },
        jpa: { // Repository read from Jpa
            repo: null,
            error: null,
            jdyI18n: []
        },
        jdyFilters: {
            allFilters: {

            }
        }
    },
    getters: {
        allJdyI18n: state => {
            return state.jpa.jdyI18n.concat(state.dynamic.jdyI18n);
        }
    },
    actions,
    mutations
});
