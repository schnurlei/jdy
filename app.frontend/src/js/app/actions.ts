import { JsonHttpObjectReader } from '@jdynameta/jdy-http';
import { convertI18nEntriesInfoI18nJson, convertRepo2I18nEntries } from '@jdynameta/jdy-i18n';
import { plantShopMessages } from '@/jdy-test';
import { i18n } from '@/i18n';
import { JdyNamedJsonFilter } from '@/js/jdy/jdy-view';

export default {

    setDynamicRepo ({ commit }, jdyRepo) {

        const entries = convertRepo2I18nEntries(jdyRepo, {});
        commit('setDynamicRepo', { repo: jdyRepo, jdyI18n: entries });

        const generatedMessages = convertI18nEntriesInfoI18nJson(entries);
        i18n.mergeLocaleMessage('de', generatedMessages['de']);
        i18n.mergeLocaleMessage('en', generatedMessages['en']);
    },

    fetchJpaRepository ({ commit }) {

        const metaReader = new JsonHttpObjectReader('/jdy/');
        return new Promise((resolve, reject) => {
            return metaReader.loadMetadataFromDb().then(repo => {
                const entries = convertRepo2I18nEntries(repo, plantShopMessages);
                commit('setJpaRepo', { repo: repo, jdyI18n: entries });
                const generatedMessages = convertI18nEntriesInfoI18nJson(entries);
                i18n.mergeLocaleMessage('de', generatedMessages['de']);
                i18n.mergeLocaleMessage('en', generatedMessages['en']);
                resolve();
            })
                .catch(data => {
                    commit('setJpaError', data);
                    reject(data);
                });
        });
    },

    addFilter  ({ commit }, filterToAdd: JdyNamedJsonFilter) {
        commit('addFilter', filterToAdd);
    }

};
