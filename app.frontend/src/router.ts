import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import JdyHolder from './components/JdyHolder.vue';
import JdyI18nEditTable from './components/i18n/JdyI18nEditTable.vue';
import store from './js/app/store';
import About from '@/views/About.vue';
import JdyPerennialExample from '@/components/JdyPerennialExample.vue';
import JdyObjectEditor from '@/components/JdyObjectEditor.vue';
Vue.use(Router);

function perennialExampleClassInfo () {

    return convertNameToClassInfo('Perennial');
}

function convertRouteToClassInfo (route) {

    return convertNameToClassInfo(route.params.classinfo);
}

function convertNameToClassInfo (name) {

    let classInfo = null;
    if (store.state.jpa.repo) {
        // @ts-ignore
        classInfo = store.state.jpa.repo.getClassInfo(name);
    }

    return {
        classinfo: classInfo
    };
}

const jdyRouter = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: About
        },
        {
            path: '/jdy/Perennial', component: JdyPerennialExample, props: perennialExampleClassInfo
        },
        {
            path: '/jdy/I18n', component: JdyI18nEditTable
        },
        {
            path: '/jdy/:classinfo', component: JdyHolder, props: convertRouteToClassInfo
        },
        {
            name: 'editValueObject',
            path: '/jdy/$edit/:classinfo',
            component: JdyObjectEditor,
            props: convertRouteToClassInfo
        }
    ]
});

jdyRouter.beforeEach((to, from, next) => {

    next();
});

export default jdyRouter;
