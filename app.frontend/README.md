# app.frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Docker build

...
docker build -t jdy-jpa-frontend .
docker run --add-host localnode:192.168.178.41 -d -p 80:80  -e "REDIRECT_HOST=http://localnode:8082/pbesdv-webservice/zsdm/v1.0/jdy/;" jdy-jpa-frontend

docker exec -it 21991560f06b bash

...