import { JdyClassInfo, JdyRepository } from '@jdynameta/jdy-base';
import {
    convertClass2I18nEntries,
    convertI18nEntriesInfoI18nJson,
    convertRepo2I18nEntries,
    JdyI18nEntry,
    Language
} from '@jdynameta/jdy-i18n';
import Vue from 'vue';
import VueI18n from 'vue-i18n';

test('JdyI18nEntry', () => {

    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    const attr = createdClass.addTextAttr('testAttr', 100);

    const i18NEntry = new JdyI18nEntry(createdClass, attr);
    i18NEntry.setTranslation(Language.de, 'Test Wert');
    i18NEntry.setTranslation(Language.en, 'Test Value');

    expect(i18NEntry.getTranslation(Language.de)).toBe('Test Wert');
});

test('convertI18nEntriesInfoI18nJson', () => {

    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    const attr = createdClass.addTextAttr('testAttr', 100);

    const i18NEntry = new JdyI18nEntry(createdClass, attr);
    i18NEntry.setTranslation(Language.de, 'Test Wert');
    i18NEntry.setTranslation(Language.en, 'Test Value');

    const messages = convertI18nEntriesInfoI18nJson([i18NEntry]);
    expect(messages['en']['testrep']['testclass']['testAttr']).toBe('Test Value');
});

test('convertRepo2I18nEntries', () => {

    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    const i18nEntries = convertRepo2I18nEntries(rep, null);
    expect(i18nEntries[0].getTranslation(Language.de)).toBe('test Attr');
});

test('convertClass2I18nEntries', () => {

    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    const i18nEntries = convertClass2I18nEntries(createdClass, null);
    expect(i18nEntries[0].getTranslation(Language.de)).toBe('test Attr');
});

test('convertClass2I18nEntriesEmptyDefault', () => {

    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    const i18nEntries = convertClass2I18nEntries(createdClass, {});
    expect(i18nEntries[0].getTranslation(Language.de)).toBe('test Attr');
});

test('use Repo with VueI18n', () => {

    Vue.use(VueI18n);
    const rep = new JdyRepository('testrep');
    const createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    const i18nEntries = convertRepo2I18nEntries(rep, null);
    const messages = convertI18nEntriesInfoI18nJson(i18nEntries);

    const i18n = new VueI18n({
        locale: 'de', // set locale
        messages // set locale messages
    });

    expect(i18n.t('testrep.testclass.testAttr')).toBe('test Attr');
});

test('vue-i18n change messages', () => {

    Vue.use(VueI18n);

    const messages = {
        en: {
            message: {
                hello: 'hello world'
            },
            message2: {
                hello2: 'hello world message 2'
            }
        },
        de: {
            message: {
                hello: 'Hallo Erde'
            },
            message2: {
                hello2: 'Hallo Erde message 2'
            }
        }
    };

    const i18n = new VueI18n({
        locale: 'de', // set locale
        messages // set locale messages
    });

    expect(i18n.t('message.hello')).toBe('Hallo Erde');
    expect(i18n.t('message2.hello2')).toBe('Hallo Erde message 2');

    const messages2 = {
        message: {
            hello: 'Hallo Erde 2'
        }
    };

    i18n.mergeLocaleMessage('de', messages2);

    expect(i18n.t('message.hello')).toBe('Hallo Erde 2');
    expect(i18n.t('message2.hello2')).toBe('Hallo Erde message 2');

});
