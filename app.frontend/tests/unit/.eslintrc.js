module.exports = {
    env: {
        jest: true
    },
    rules: {
        'import/no-extraneous-dependencies': 'off'
    },
    globals: {
        isChrome: true,
        waitForUpdate: true,
        nextTick: true,
        delay: true,
        assert: true,
        sinon: true,
        Vue: true,
        VueI18n: true
    }
};
