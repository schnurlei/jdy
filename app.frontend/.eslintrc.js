module.exports = {
    root: true,
    env: {
        node: true
    },
    'extends': [
        'plugin:vue/base',
        '@vue/standard',
        '@vue/typescript',
        'plugin:@typescript-eslint/recommended'
    ],
    rules: {
        'indent': ['error', 4, { 'SwitchCase': 1 }],
        'operator-linebreak': ['warn', 'after'],
        'no-use-before-define': ['warn', { 'functions': true, 'classes': false }],
        '@typescript-eslint/no-use-before-define': ['warn', { 'functions': true, 'classes': false }],
        'semi': [1, 'always'],
        'padded-blocks': ['off', { 'blocks': 'always' }],
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'vue/v-bind-style': ['warn', 'shorthand'],
        '@typescript-eslint/ban-ts-ignore': ['off', 'always'],
        '@typescript-eslint/no-explicit-any': ['off', 'always']
    },
    'parser': 'vue-eslint-parser',
    'parserOptions': {
        'parser': '@typescript-eslint/parser'
    },
    plugins: ['@typescript-eslint']
};
