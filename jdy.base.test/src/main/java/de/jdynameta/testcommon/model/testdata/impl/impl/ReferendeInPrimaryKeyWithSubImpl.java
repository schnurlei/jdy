/**
 *
 * Copyright 2011 (C) Rainer Schneider,Roggenburg <schnurlei@googlemail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jdynameta.testcommon.model.testdata.impl.impl;


/**
 * ReferendeInPrimaryKeyWithSubImpl
 *
 * @author Copyright &copy;  
 * @author Rainer Schneider
 * @version 
 */
public  class ReferendeInPrimaryKeyWithSubImpl extends de.jdynameta.base.value.defaultimpl.ReflectionValueObject

{
	private java.lang.Long simpleIntKey;
	private SimpleKeyObjImpl simpleRefKey;
	private CompoundKeyObjImpl compoundRefKey;
	private java.lang.String referenceData;

	/**
	 *Constructor 
	 */
	public ReferendeInPrimaryKeyWithSubImpl ()
	{
		super(new de.jdynameta.base.value.DefaultClassNameCreator());
	}

	/**
	 * Get the simpleIntKey

	 * @return get the simpleIntKey
	 */
	public Long getSimpleIntKey() 
	{
		return simpleIntKey;
	}

	/**
	 * set the simpleIntKey

	 * @param simpleIntKey
	 */
	public void setSimpleIntKey( Long aSimpleIntKey) 
	{
		simpleIntKey = aSimpleIntKey;
	}

	/**
	 * Get the simpleRefKey

	 * @return get the simpleRefKey
	 */
	public SimpleKeyObjImpl getSimpleRefKey() 
	{
		return simpleRefKey;
	}

	/**
	 * set the simpleRefKey

	 * @param simpleRefKey
	 */
	public void setSimpleRefKey( SimpleKeyObjImpl aSimpleRefKey) 
	{
		simpleRefKey = aSimpleRefKey;
	}

	/**
	 * Get the compoundRefKey

	 * @return get the compoundRefKey
	 */
	public CompoundKeyObjImpl getCompoundRefKey() 
	{
		return compoundRefKey;
	}

	/**
	 * set the compoundRefKey

	 * @param compoundRefKey
	 */
	public void setCompoundRefKey( CompoundKeyObjImpl aCompoundRefKey) 
	{
		compoundRefKey = aCompoundRefKey;
	}

	/**
	 * Get the referenceData

	 * @return get the referenceData
	 */
	public String getReferenceData() 
	{
		return referenceData;
	}

	/**
	 * set the referenceData

	 * @param referenceData
	 */
	public void setReferenceData( String aReferenceData) 
	{
		referenceData = (aReferenceData!= null) ? aReferenceData.trim() : null;
	}

	/* (non-Javadoc)
	 *  java.lang.Object#equals(java.lang.Object
	 */
	@Override
	public boolean equals(Object compareObj) 
	{
		ReferendeInPrimaryKeyWithSubImpl typeObj = (ReferendeInPrimaryKeyWithSubImpl) compareObj;
		return typeObj != null 
				&& ( 
					( 
					(getSimpleIntKey() != null
					&& typeObj.getSimpleIntKey() != null
					&& this.getSimpleIntKey().equals( typeObj.getSimpleIntKey()) )

					&& (getSimpleRefKey() != null
					&& typeObj.getSimpleRefKey() != null
					&& typeObj.getSimpleRefKey().equals( typeObj.getSimpleRefKey()) )

					&& (getCompoundRefKey() != null
					&& typeObj.getCompoundRefKey() != null
					&& typeObj.getCompoundRefKey().equals( typeObj.getCompoundRefKey()) )
					)
					|| ( getSimpleIntKey() == null
					&& typeObj.getSimpleIntKey() == null
					&& getSimpleRefKey() == null
					&& typeObj.getSimpleRefKey() == null
					&& getCompoundRefKey() == null
					&& typeObj.getCompoundRefKey() == null
					&& this == typeObj )
				);
	}
	/* (non-Javadoc)
	 *  java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		return 				 (( simpleIntKey != null) ? simpleIntKey.hashCode() : super.hashCode())
			^
				 (( simpleRefKey != null) ? simpleRefKey.hashCode() : super.hashCode())
			^
				 (( compoundRefKey != null) ? compoundRefKey.hashCode() : super.hashCode())		;
	}

}
