package de.jdynameta.jdy.spring.app.data;

import de.jdynameta.jdy.model.jpa.example.plexx.PersonImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<PersonImpl, Long> {
}
