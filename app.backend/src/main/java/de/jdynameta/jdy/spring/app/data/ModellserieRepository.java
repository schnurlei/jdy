package de.jdynameta.jdy.spring.app.data;

import de.jdynameta.jdy.model.jpa.example.plexx.ModellserieImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModellserieRepository extends CrudRepository<ModellserieImpl, Long> {
}
