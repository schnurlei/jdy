package de.jdynameta.jdy.spring.app.data;

import de.jdynameta.jdy.model.jpa.example.Plant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantRepository extends CrudRepository<Plant, Integer> {

    public static Plant insertPlantHyssopus(PlantRepository repo) {

        Plant newPlant = new Plant();

        newPlant.setBotanicName("Hyssopus officinalis");
        newPlant.setColor("blue");
        newPlant.setHeigthInCm(50);
        newPlant.setPlantFamily(Plant.PlantFamily.Lamiaceae);

        repo.save(newPlant);
        return newPlant;
    }

    public static Plant createPlantIris() {

        Plant newPlant = new Plant();

        newPlant.setBotanicName("Iris barbata-elatior 'Natchez Trace'");
        newPlant.setColor("H kastanienbraun, D apricot ");
        newPlant.setHeigthInCm(80);
        newPlant.setPlantFamily(Plant.PlantFamily.Iridaceae);

        return newPlant;
    }

    public static Plant insertPlantDahlia(PlantRepository repo, byte[] picture) {

        Plant newPlant = new Plant();

        newPlant.setBotanicName("Dahlia 'Marie Schnugg'");
        newPlant.setColor("rot");
        newPlant.setHeigthInCm(80);
        newPlant.setPlantFamily(Plant.PlantFamily.Asteraceae);
        newPlant.setPicture(picture);

        repo.save(newPlant);
        return newPlant;
    }

}
