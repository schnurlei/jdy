package de.jdynameta.jdy.spring.app.data;

import de.jdynameta.jdy.model.jpa.example.plexx.MotorImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MotorRepository extends CrudRepository<MotorImpl, Long> {
}
