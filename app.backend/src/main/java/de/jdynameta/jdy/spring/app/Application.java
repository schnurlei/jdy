package de.jdynameta.jdy.spring.app;

import de.jdynameta.jdy.model.jpa.entity.Landkreis;
import de.jdynameta.jdy.model.jpa.entity.Teilnehmer;
import de.jdynameta.jdy.model.jpa.entity.Veranstaltung;
import de.jdynameta.jdy.model.jpa.example.Plant;
import de.jdynameta.jdy.model.jpa.example.Plantorder;
import de.jdynameta.jdy.spring.app.config.JpaConfiguration;
import de.jdynameta.jdy.spring.app.data.*;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

import javax.persistence.EntityManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
@Import(JpaConfiguration.class)
public class Application {

	static final Logger LOG = LoggerFactory.getLogger(Application.class);

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private TeilnehmerRepository teilnehmerRepo;

	@Autowired
	private VeranstaltungRepository veranstaltungRepo;

	@Autowired
	private PlantRepository plantRepo;

	@Autowired
	private PlantOrderRepository plantOrderRepo;

	@Autowired
	private PlantOrderItemRepository itemRepo;

	@Autowired
	private FahrzeugRepository fahrzeugRepo;

	@Autowired
	private PersonRepository personRepo;

	@Autowired
	private ModellserieRepository modelSerieRepo;

	@Autowired
	private MotorRepository motorRepo;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@PostConstruct
	private void init() {

		insertTeilnehmer("Hans");
		insertTeilnehmer("Franz");
		Plant hyssopus = PlantRepository.insertPlantHyssopus(this.plantRepo);
		Plant iris = PlantRepository.createPlantIris();
		try {

			iris.setPicture(getPicture("iris_orange_rot.png"));
			this.plantRepo.save(iris);
			PlantRepository.insertPlantDahlia(this.plantRepo, getPicture("Dalia.JPG"));
		} catch (IOException | NullPointerException ex) {
			LOG.error("Error setting plant picture", ex);
		}
		Plantorder createdOrder = PlantOrderRepository.createPlantorder(this.plantOrderRepo, this.itemRepo, hyssopus, iris);


		DataGenerator.createFahrzeug(this.fahrzeugRepo
				, DataGenerator.createPerson(this.personRepo)
				, DataGenerator.createModellserie(this.modelSerieRepo)
				, DataGenerator.createMotor(this.motorRepo));
	}

	private byte[] getPicture(String pictureName) throws IOException {
		return readAllBytes(new ClassPathResource(pictureName, Application.class).getInputStream());
	}

	private byte[] readAllBytes(InputStream in) throws IOException {

		// Java 9
		//in.readAllBytes())
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[4096];
		for (;;) {
			int nread = in.read(buffer);
			if (nread <= 0) {
				break;
			}
			baos.write(buffer, 0, nread);
		}
		return baos.toByteArray();
	}
	public void insertTeilnehmer(String name) {

		final Veranstaltung veranstaltung = new Veranstaltung();
		veranstaltung.setBeschreibung(name + "_Veranstaltung_Beschreibung");
		veranstaltung.setDatum(new Date());
		veranstaltung.setChangable(true);

		Teilnehmer teilnehmer = new Teilnehmer();
		teilnehmer.setName(name);
		teilnehmer.setBeschreibung(name + "_Beschreibung");
		teilnehmer.setFreigegeben(true);
		teilnehmer.setLandkreis(Landkreis.AIC);
		teilnehmer.setLat(new BigDecimal(12.33));
		teilnehmer.setLng(new BigDecimal(77.22));
		teilnehmer.setOrt("TestOrt");
		teilnehmer.setPlz(89277);
//		teilnehmer.setVeranstaltung(veranstaltung);

		this.veranstaltungRepo.save(veranstaltung);
		this.teilnehmerRepo.save(teilnehmer);
	}

}
