package de.jdynameta.jdy.spring.app.data;

import de.jdynameta.jdy.model.jpa.example.plexx.*;

import java.math.BigDecimal;


public class DataGenerator {


    public static MotorImpl createMotor(final MotorRepository repo) {

        MotorImpl newMotor = new MotorImpl();

        newMotor.setId(1L);
        newMotor.setBezeichnung("Moto V8");

        newMotor.setHersteller(Hersteller.AUDI);
        newMotor.setLeistungInKwh(new BigDecimal("140.4"));

        repo.save(newMotor);
        return newMotor;
    }

    public static ModellserieImpl createModellserie(final ModellserieRepository repo) {

        ModellserieImpl newModellserie = new ModellserieImpl();

        newModellserie.setId(1L);
        newModellserie.setBezeichnung("Modellserie V");

        newModellserie.setName("Name V");
        newModellserie.setStatus(ModellStatus.AKTUELL);

        repo.save(newModellserie);
        return newModellserie;
    }

    public static PersonImpl createPerson(final PersonRepository repo) {

        PersonImpl newPerson = new PersonImpl();

        newPerson.setId(1L);
        newPerson.setAdresse("Musterstadt");

        newPerson.setNachname("Musterman");
        newPerson.setVorname("Max");

        repo.save(newPerson);
        return newPerson;
    }

    public static FahrzeugImpl createFahrzeug(final FahrzeugRepository repo, Person besitzer, Modellserie serie
            , Motor motor) {

        FahrzeugImpl newFahrzeug = new FahrzeugImpl();

        newFahrzeug.setId(1L);
        newFahrzeug.setBesitzer(besitzer);

        newFahrzeug.setHersteller(Hersteller.AUDI);
        newFahrzeug.setModellserie(serie);
        newFahrzeug.setMotor(motor);

        repo.save(newFahrzeug);
        return newFahrzeug;
    }

}
