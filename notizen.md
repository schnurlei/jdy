## edit passphrase

As of gpg version 2.2.17, gpg --edit-key <keyid> seems to work fine for removing a passphrase.

Issue the command, then type passwd in the prompt. It will ask you to provide your current passphrase and then the new one. Just type Enter for no passphrase. Then type quit to quit the program.

## edit settings xml
