module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  env: {
    node: true
  },
  plugins: ['@typescript-eslint'],
  'extends': [
    "eslint:recommended",
    'plugin:@typescript-eslint/recommended',
    "plugin:@typescript-eslint/recommended"
  ],
  rules: {
    "indent": ["error", 4, { "SwitchCase": 1 }],
    "operator-linebreak": ["warn", "after"],
    "no-use-before-define": ["warn", { "functions": true, "classes": false }],
    "@typescript-eslint/no-use-before-define": ["warn", { "functions": true, "classes": false }],
    'semi': [1, "always"],
    'padded-blocks': ['off', { 'blocks': 'always' }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  parserOptions: {
  }
}
