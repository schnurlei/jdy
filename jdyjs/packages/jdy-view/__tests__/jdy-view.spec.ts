import {
    JdyAndExpression,
    JdyClassInfo,
    JdyClassInfoQuery, JdyGreatorOperator, JdyOperatorExpression,
    JdyQueryCreator,
    JdyTypedValueObject,
    ObjectFilterExpression
} from '@jdynameta/jdy-base';
import {testCreatePlantShopRepository} from '@jdynameta/jdy-test';
import {createFilterRepository, FilterCreator, getDefaultFilterNameMapping} from '@jdynameta/jdy-meta';
import {JsonCompactFileReader, JsonCompactFileWriter, JsonFileReader, Operation} from '@jdynameta/jdy-json';
import {convertQueryToJsonString} from '../src/jdy-view';

test(' convert and expr + 1 operator filter to json ', function () {

    const repo = testCreatePlantShopRepository();
    const plantType: JdyClassInfo | null = testCreatePlantShopRepository().getClassInfo('Plant');

    // @ts-ignore
    let query : JdyClassInfoQuery  = new JdyQueryCreator(plantType)
        .and()
        .greater('heigthInCm', 30)
        .end()
        .query();

    const filterJsonString = convertQueryToJsonString(query);
    expect(filterJsonString).toBe('[{"@t":"FQM","rn":"TestApp","cn":"Plant","ex":{"@t":"FEA","ase":[{"@t":"OEX","an":"heigthInCm","op":{"@t":"FPG","ae":false},"lv":30}]},"selectAttributes":[]}]');

    const appQueryInfo: JdyClassInfo | null = createFilterRepository().getClassInfo('AppQuery');
    const filterObjectVo: JdyTypedValueObject[] = new JsonCompactFileReader(getDefaultFilterNameMapping()).readObjectList(JSON.parse(filterJsonString), appQueryInfo);
    const filterObject: JdyClassInfoQuery = new FilterCreator().convertAppFilter2MetaFilter(filterObjectVo[0], repo);

    expect(filterObject.getFilterExpression()).toBeInstanceOf(JdyAndExpression);
    const andExpr1 = filterObject.getFilterExpression() as JdyAndExpression;
    expect(andExpr1.expressionVect[0]).toBeInstanceOf(JdyOperatorExpression);
    const operatorExpr1 = andExpr1.expressionVect[0] as JdyOperatorExpression;
    expect(operatorExpr1.getOperator()).toBeInstanceOf(JdyGreatorOperator);
    expect(operatorExpr1.compareValue).toEqual(30);
    expect(operatorExpr1.attributeInfo.getInternalName()).toEqual('heigthInCm');

});

