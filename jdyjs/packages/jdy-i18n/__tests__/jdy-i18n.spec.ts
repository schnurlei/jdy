import {JdyClassInfo, JdyRepository} from '@jdynameta/jdy-base';
import {
    convertClass2I18nEntries,
    convertI18nEntriesInfoI18nJson,
    convertRepo2I18nEntries,
    JdyI18nEntry,
    Language
} from '../src/jdy-i18n';

test('JdyI18nEntry', () => {

    let rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    let attr = createdClass.addTextAttr('testAttr', 100);

    let i18NEntry = new JdyI18nEntry(createdClass, attr);
    i18NEntry.setTranslation(Language.de, "Test Wert");
    i18NEntry.setTranslation(Language.en, "Test Value");

    expect(i18NEntry.getTranslation(Language.de)).toBe('Test Wert');
});

test('convertI18nEntriesInfoI18nJson', () => {

    let rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    let attr = createdClass.addTextAttr('testAttr', 100);

    let i18NEntry = new JdyI18nEntry(createdClass, attr);
    i18NEntry.setTranslation(Language.de, "Test Wert");
    i18NEntry.setTranslation(Language.en, "Test Value");

    let messages = convertI18nEntriesInfoI18nJson([i18NEntry]);
    expect(messages["en"]["testrep"]["testclass"]["testAttr"]).toBe("Test Value");
});

test('convertRepo2I18nEntries', () => {

    let rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    let i18nEntries = convertRepo2I18nEntries(rep, null);
    expect(i18nEntries[0].getTranslation(Language.de)).toBe("test Attr");
});


test('convertClass2I18nEntries', () => {

    let rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    let i18nEntries = convertClass2I18nEntries(createdClass, null);
    expect(i18nEntries[0].getTranslation(Language.de)).toBe("test Attr");
});

test('convertClass2I18nEntriesEmptyDefault', () => {

    let rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo = rep.addClassInfo('testclass', null);
    createdClass.addTextAttr('testAttr', 100);

    let i18nEntries = convertClass2I18nEntries(createdClass, {});
    expect(i18nEntries[0].getTranslation(Language.de)).toBe("test Attr");
});


