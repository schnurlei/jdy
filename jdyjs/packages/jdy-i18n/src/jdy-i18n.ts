import { JdyAttributeInfo, JdyClassInfo, JdyRepository } from '@jdynameta/jdy-base';

export enum Language {
    de= 'de', // german
    en = 'en', // english
    fr = 'fr', // French
    es = 'es' // Spain
}

export class JdyI18nEntry {

    private classInfo: JdyClassInfo;
    private attribute: JdyAttributeInfo;
    private translations: { [language: string]: string };

    public constructor (classInfo: JdyClassInfo, attribute: JdyAttributeInfo) {

        this.classInfo = classInfo;
        this.attribute = attribute;
        this.translations = {};
    }

    public get repoName (): string {
        return this.classInfo.getRepoName();
    }

    public get className (): string {
        return this.classInfo.getInternalName();
    }

    public get attributeName (): string {
        return this.attribute.getInternalName();
    }

    public getTranslation (lang: string): string {
        return this.translations[lang];
    }

    public setTranslation (lang: Language, text: string) {
        this.translations[lang] = text;
    }

    public setTranslationText (lang: string, text: string) {
        this.translations[lang] = text;
    }
}

export function convertRepo2I18nEntries (repo: JdyRepository, defaultMessages: null | any): JdyI18nEntry[] {

    let allEntries: JdyI18nEntry[] = [];

    repo.getClasses()
        .forEach(jdyClass => jdyClass
            .forEachAttr(attr => {
                const newEntry = new JdyI18nEntry(jdyClass, attr);
                newEntry.setTranslation(Language.de, getTranslation(newEntry, 'de', defaultMessages));
                newEntry.setTranslation(Language.en, getTranslation(newEntry, 'en', defaultMessages));
                allEntries.push(newEntry);
            }));

    return allEntries;
}

export function convertClass2I18nEntries (jdyClass: JdyClassInfo, defaultMessages: null | any): JdyI18nEntry[] {

    let allEntries: JdyI18nEntry[] = [];
    jdyClass
        .forEachAttr(attr => {
            const newEntry = new JdyI18nEntry(jdyClass, attr);
            newEntry.setTranslation(Language.de, getTranslation(newEntry, 'de', defaultMessages));
            newEntry.setTranslation(Language.en, getTranslation(newEntry, 'en', defaultMessages));
            allEntries.push(newEntry);
        });
    return allEntries;
}

function getTranslation (entry: JdyI18nEntry, lang: string, defaultMessages: null | any) {

    return (defaultMessages &&
        defaultMessages[lang] &&
        defaultMessages[lang][entry.repoName] &&
        defaultMessages[lang][entry.repoName][entry.className] &&
        defaultMessages[lang][entry.repoName][entry.className][entry.attributeName])
        ? defaultMessages[lang][entry.repoName][entry.className][entry.attributeName]
        : splitStringByUppercase(entry.attributeName);
}

function splitStringByUppercase (stringToSplit: string): string {

    const splitted = stringToSplit.match(/[A-Z]*[^A-Z]+/g);
    if (splitted) {
        return splitted.join(' ');
    } else {
        return stringToSplit;
    }
}

export function convertI18nEntriesInfoI18nJson (allEntries: JdyI18nEntry[]) {

    let messages = {};
    allEntries.forEach(entry => {

        for (const lang in Language) {
            if (!Number(lang)) {

                if (!messages[lang]) {
                    messages[lang] = {};
                }

                if (!messages[lang][entry.repoName]) {
                    messages[lang][entry.repoName] = {};
                }

                if (!messages[lang][entry.repoName][entry.className]) {
                    messages[lang][entry.repoName][entry.className] = {};
                }

                messages[lang][entry.repoName][entry.className][entry.attributeName] = entry.getTranslation(lang);
            }
        }
    });

    return messages;
}

export async function yandexTranslate(text, language) {

    const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key='
        + 'trnsl.1.1.20191118T205115Z.ce76e1325d1fe95b.12260b80c78726a15514aab57891025a39744841'
        + '&lang='
        + encodeURIComponent(language)
        + '&text='
        + encodeURIComponent(text);

    return fetch(url, {
            mode: 'cors',
            headers: {
                'Access-Control-Allow-Origin':'*'
            }
        } )
        .then(response => response.json());
}
