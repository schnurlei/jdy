import {
    JdyClassInfo,
    JdyRepository,
} from '../src/index';

test(' Repository creation', () => {

    const rep = new JdyRepository('testrep');
    let createdClass: JdyClassInfo | null = rep.addClassInfo('testclass', null);
    expect(createdClass.getInternalName()).toBe('testclass'); // 'Class name exits'
    expect(createdClass.getRepoName()).toBe('testrep'); //  'namespace exists'

    expect(() => { rep.addClassInfo('testclass', null); })
        .toThrow('Class already exists with name: testcla');

    createdClass = rep.getClassInfo('testclass');
    expect(createdClass).not.toBeNull();
    if (createdClass !== null) {
        expect(createdClass.getInternalName()).toBe('testclass'); // 'Class name exits'
        expect(createdClass.getRepoName()).toBe('testrep'); // 'namespace exists'
    }
});

test(' Add attributes to class', () => {

    const rep = new JdyRepository('testrep');
    const createdClass = rep.addClassInfo('testclass', null);
    const createdTextAttr = createdClass.addTextAttr('testTextAttr', 99);
    const createdBooleanAttr = createdClass.addBooleanAttr('testBooleanAttr');
    const createdVarCharAttr = createdClass.addVarCharAttr('testVarCharAttr', 22);
    const createdDecimalAttr = createdClass.addDecimalAttr('testDecimalAttr', -10.22, 10.44, 3);
    const createdLongAttr = createdClass.addLongAttr('testLongAttr', -100, 1000);
    const createdTimeStampAttr = createdClass.addTimeStampAttr('testTimeStampAttr', true, true);
    const createdFloatAttr = createdClass.addBooleanAttr('testFloatAttr');
    const createdBlobAttr = createdClass.addBlobAttr('testBlobAttr');

    expect(createdTextAttr.getInternalName()).toBe('testTextAttr'); // 'Text Attribute exits');
    expect(createdBooleanAttr.getInternalName()).toBe('testBooleanAttr'); // 'Boolean Attribute exits');
    expect(createdVarCharAttr.getInternalName()).toBe('testVarCharAttr'); // 'VarChar Attribute exits');
    expect(createdDecimalAttr.getInternalName()).toBe('testDecimalAttr'); // 'Decimal Attribute exits');
    expect(createdLongAttr.getInternalName()).toBe('testLongAttr'); // 'Long Attribute exits');
    expect(createdTimeStampAttr.getInternalName()).toBe('testTimeStampAttr'); // 'TimeStamp Attribute exits');
    expect(createdFloatAttr.getInternalName()).toBe('testFloatAttr'); // 'Float Attribute exits');
    expect(createdBlobAttr.getInternalName()).toBe('testBlobAttr'); // 'Blob Attribute exits');

    expect(() => { createdClass.addTextAttr('testBooleanAttr', null); })
        .toThrow('Attribute already exists with name: testBooleanAttr');

});

test(' Add association to class', function () {

    const rep = new JdyRepository('testrep');
    const masterclass = rep.addClassInfo('Masterclass', null);
    const detailclass = rep.addClassInfo('Detailclass', null);
    rep.addAssociation('AssocName', masterclass, detailclass, 'Masterclass', 'Masterclass', true, true, true);
    const assoc = masterclass.getAssoc('AssocName');
    const backReference = detailclass.getAttr('Masterclass');
    expect(assoc.getAssocName()).toBe('AssocName'); // 'Association exits');
    expect(backReference.getInternalName()).toBe('Masterclass'); // 'Backreference exits');

});

test(' Subclass creation', () => {

    const rep = new JdyRepository('testrep');
    const baseClass = rep.addClassInfo('baseClass', null);
    const subClass1 = rep.addClassInfo('subClass1', baseClass);
    const subClass2 = rep.addClassInfo('subClass2', subClass1);

    baseClass.addTextAttr('baseTextAttr', 50);
    subClass1.addTextAttr('sub1TextAttr', 100);
    subClass2.addTextAttr('sub2TextAttr', 100);

    const allAttr = subClass2.getAllAttributeList();

    expect(allAttr.length).toBe(3); // 'Sublass has aslo Suberclass attributes');

});


