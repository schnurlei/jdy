import construct = Reflect.construct;

export class JdyValidationError extends Error {
    public constructor (m: string) {
        super(m);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, JdyValidationError.prototype);
    }
}

export class JdyRepository {

    private _repoName: string;
    private classes: { [name: string]: JdyClassInfo };

    public constructor (repoName: string) {

        this._repoName = repoName;
        this.classes = {};
    }

    public get i18n () {

        return this.repoName;
    }

    public get repoName () {
        return this._repoName;
    }

    public getClasses (): JdyClassInfo[] {

        return Object.values(this.classes);
    }

    public addClassInfo (aInternalName, aSuperclass: JdyClassInfo | null): JdyClassInfo {

        if (this.classes[aInternalName]) {

            throw new JdyValidationError('Class already exists with name: ' + aInternalName);
        }

        const newClass = new JdyClassInfo(this.repoName, aInternalName, aSuperclass);
        this.classes[aInternalName] = newClass;
        return newClass;
    }

    public getClassInfo (aInternalName): JdyClassInfo | null {

        return this.classes[aInternalName];
    }

    public addAssociation (anAssocName: string, aMasterClass: JdyClassInfo, aDetailClass: JdyClassInfo
        , aDetailInternalName: string, aDetailExternalName: string
        , keyInDetail: boolean, notNullInDetail: boolean, aIsDependent: boolean) {

        const detailToMasterAssoc = new JdyObjectReferenceInfo(aDetailInternalName, aDetailExternalName,
            keyInDetail, notNullInDetail, aMasterClass);
        detailToMasterAssoc.i18n = this.i18n + '.' + aDetailInternalName;
        detailToMasterAssoc.setIsDependent(aIsDependent);
        detailToMasterAssoc.setIsInAssociation(true);
        aDetailClass.addReferenceToAttrList(detailToMasterAssoc);
        const newAssoc = new JdyAssociationModel(detailToMasterAssoc, aDetailClass, anAssocName);
        aMasterClass.addAssociation(newAssoc);
    }
}

export class JdyClassInfo {

    private nameSpace: string | null;
    private repoName: string;
    private internalName: string;
    private externalName: string;
    private shortName: string;
    private isAbstract: boolean;
    private superclass: JdyClassInfo | null;
    private attributes: { [name: string]: JdyAttributeInfo };
    private attrList: JdyAttributeInfo[];
    private associations: { [name: string]: JdyAssociationModel };
    private assocList: JdyAssociationModel[];
    private subclasses: JdyClassInfo[];
    private standalone: boolean;

    public constructor (repoName: string, internalName: string, superclass: JdyClassInfo | null) {
        this.nameSpace = null;
        this.repoName = repoName;
        this.internalName = internalName;
        this.superclass = superclass;
        if (this.superclass) {
            this.superclass.subclasses.push(this);
        }

        this.shortName = internalName;
        this.externalName = internalName;
        this.attributes = {};
        this.attrList = [];
        this.associations = {};
        this.assocList = [];
        this.subclasses = [];
        this.isAbstract = false;
        this.standalone = true;
    }

    public get i18n () {

        return this.repoName + '.' + this.internalName;
    }

    public forEachAttr (callbackfn: (value: JdyAttributeInfo, index: number, array: JdyAttributeInfo[]) => void, thisArg?: any) {
        this.getAllAttributeList().forEach(callbackfn);
    }

    public forEachAssoc (callbackfn: (value: JdyAssociationModel, index: number, array: JdyAssociationModel[]) => void, thisArg?: any) {
        this.getAllAssociationList().forEach(callbackfn);
    }

    public getAllAttributeList (): JdyAttributeInfo[] {
        let tmpAttrList: JdyAttributeInfo[] = this.attrList;

        if (this.superclass) {
            tmpAttrList = tmpAttrList.concat(this.superclass.getAllAttributeList());
        }

        return tmpAttrList;
    }

    public getAllAssociationList (): JdyAssociationModel[] {
        let tmpAssocList: JdyAssociationModel[] = this.assocList;

        if (this.superclass) {
            tmpAssocList = tmpAssocList.concat(this.superclass.getAllAssociationList());
        }

        return tmpAssocList;
    }

    public addAssociation (aAssoc: JdyAssociationModel) {
        if (this.associations[aAssoc.getAssocName()]) {
            throw new JdyValidationError('Associtaion already exists with name: ' + aAssoc.getAssocName());
        }

        this.associations[aAssoc.getAssocName()] = aAssoc;
        this.assocList.push(aAssoc);
        return aAssoc;
    }

    public addReference (anInternalName, aReferencedClass) {

        const newAttr = new JdyObjectReferenceInfo(anInternalName, anInternalName, false, false, aReferencedClass);
        newAttr.i18n = this.i18n + '.' + anInternalName;
        this.addReferenceToAttrList(newAttr);
        return newAttr;
    }

    public addReferenceToAttrList (aObjRef) {
        if (this.attributes[aObjRef.getInternalName()]) {
            throw new JdyValidationError('Attribute already exists with name: ' + aObjRef.getInternalName());
        }

        this.attributes[aObjRef.getInternalName()] = aObjRef;
        this.attrList.push(aObjRef);
    }

    public addPrimitiveAttr (aInternalName: string, aPrimitiveType) {

        if (this.attributes[aInternalName]) {
            throw new Error('Attribute already exists with name: ' + aInternalName);
        }

        const newAttr = new JdyPrimitiveAttributeInfo(aInternalName, aInternalName, false, false, aPrimitiveType);
        newAttr.i18n = this.i18n + '.' + aInternalName;
        this.attributes[aInternalName] = newAttr;
        this.attrList.push(newAttr);
        return newAttr;
    }

    public addSubclass (aSubclass): void {

        this.subclasses.push(aSubclass);
    }

    public addTextAttr (aInternalName, aLength, aDomainValueList?: null | string[]) {
        return this.addPrimitiveAttr(aInternalName, new JdyTextType(aLength, null, aDomainValueList));
    }

    public addEmailAttr (aInternalName, aLength, aDomainValueList) {
        return this.addPrimitiveAttr(aInternalName, new JdyTextType(aLength, JdyTextTypeHint.EMAIL, aDomainValueList));
    }

    public addUrlAttr (aInternalName, aLength, aDomainValueList) {
        return this.addPrimitiveAttr(aInternalName, new JdyTextType(aLength, JdyTextTypeHint.URL, aDomainValueList));
    }

    public addTelephoneAttr (aInternalName, aLength, aDomainValueList) {
        return this.addPrimitiveAttr(aInternalName, new JdyTextType(aLength, JdyTextTypeHint.TELEPHONE, aDomainValueList));
    }

    public addBooleanAttr (aInternalName) {
        return this.addPrimitiveAttr(aInternalName, new JdyBooleanType());
    }

    public addVarCharAttr (aInternalName, aLength) {
        return this.addPrimitiveAttr(aInternalName, new JdyVarCharType(aLength, '', false));
    }

    public addDecimalAttr (aInternalName, aMinValue, aMaxValue, aScale, aDomainValueList = null) {
        return this.addPrimitiveAttr(aInternalName, new JdyDecimalType(aMinValue, aMaxValue, aScale, aDomainValueList));
    }

    public addLongAttr (aInternalName, aMinValue, aMaxValue, aDomainValueList = null) {
        return this.addPrimitiveAttr(aInternalName, new JdyLongType(aMinValue, aMaxValue, aDomainValueList));
    }

    public addTimeStampAttr (aInternalName, isDatePartUsed, isTimePartUsed) {
        return this.addPrimitiveAttr(aInternalName, new JdyTimeStampType(isDatePartUsed, isTimePartUsed));
    }

    public addFloatAttr (aInternalName) {
        return this.addPrimitiveAttr(aInternalName, new JdyFloatType());
    }

    public addBlobAttr (aInternalName) {
        return this.addPrimitiveAttr(aInternalName, new JdyBlobType(''));
    }

    public getShortName () {
        return this.shortName;
    }

    public setShortName (aShortName) {
        this.shortName = aShortName;
        return this;
    }

    public setAbstract (newValue) {
        this.isAbstract = newValue;
        return this;
    }

    public getStandalone () {
        return this.standalone;
    }

    public setStandalone (newValue: boolean) {
        this.standalone = newValue;
        return this;
    }

    public getInternalName () {
        return this.internalName;
    }

    public getAssoc (aAssocName) {
        return this.associations[aAssocName];
    }

    public getAttr (aInternalName): JdyAttributeInfo {
        return this.attributes[aInternalName];
    }

    public getNameSpace () {
        return this.nameSpace;
    }

    public getRepoName () {
        return this.repoName;
    }

    public getAllSubclasses () {
        return this.subclasses;
    }
}

export class JdyAssociationModel {

    private detailClass: JdyClassInfo;
    private assocName: string;
    private masterClassReference: JdyObjectReferenceInfo;

    public constructor (aMasterClassRef: JdyObjectReferenceInfo, aDetailClass: JdyClassInfo, anAssocName: string) {
        this.detailClass = aDetailClass;
        this.masterClassReference = aMasterClassRef;
        this.assocName = anAssocName;
    }

    public getAssocName () {
        return this.assocName;
    }

    public getDetailClass () : JdyClassInfo {
        return this.detailClass;
    }

    public getMasterClassReference () {
        return this.masterClassReference;
    }

}

export class JdyAttributeInfo {

    private internalName: string;
    private externalName: string;
    private key: boolean;
    private isNotNull: boolean;
    private generated: boolean;
    private attrGroup: string | null;
    private pos: null;
    protected primitive: boolean;
    private _i18n;

    public constructor (aInternalName: string, aExternalName: string, isKey: boolean, isNotNull: boolean) {
        this.internalName = aInternalName;
        this.externalName = aExternalName;
        this.key = isKey;
        this.isNotNull = isNotNull;
        this.generated = false;
        this.attrGroup = null;
        this.pos = null;
        this.primitive = false;
    }

    public getInternalName () {
        return this.internalName;
    }

    public isPrimitive () {
        return (this.primitive) ? this.primitive : false;
    }

    public isKey () {
        return (this.key) ? this.key : false;
    }

    public isGenerated () {
        return (this.generated) ? this.generated : false;
    }

    public setIsKey (isKey) {
        this.key = isKey;
        return this;
    }

    public getNotNull () {
        return (this.isNotNull) ? this.isNotNull : false;
    }

    public setNotNull (isNotNull) {
        this.isNotNull = isNotNull;
        return this;
    }

    public setGenerated (isGenerated) {
        this.generated = isGenerated;
        return this;
    }

    public setExternalName (aExternalName) {
        this.externalName = aExternalName;
        return this;
    }

    public setAttrGroup (anAttrGroup) {
        this.attrGroup = anAttrGroup;
        return this;
    }

    public setPos (aPos) {
        this.pos = aPos;
        return this;
    }

    public get i18n () {
        return this._i18n;
    }

    public set i18n (value) {
        this._i18n = value;
    }
}

export class JdyObjectReferenceInfo extends JdyAttributeInfo {
    private referencedClass: JdyClassInfo;
    private inAssociation: boolean;
    private dependent: boolean;

    public constructor (aInternalName: string, aExternalName: string, isKeyFlag: boolean, isNotNullFlag: boolean, aReferencedClass: JdyClassInfo) {
        super(aInternalName, aExternalName, isKeyFlag, isNotNullFlag);
        this.referencedClass = aReferencedClass;
        this.inAssociation = false;
        this.dependent = false;
        this.primitive = false;
    }

    public getReferencedClass () {
        return this.referencedClass;
    }

    public setIsDependent (isDependent) {
        this.dependent = isDependent;
        return this;
    }

    public setIsInAssociation (inAssociation) {
        this.inAssociation = inAssociation;
        return this;
    }
}

export class JdyPrimitiveAttributeInfo extends JdyAttributeInfo {

    private type: JdyPrimitiveType;

    public constructor (aInternalName: string, aExternalName: string, isKey: boolean, isNotNull: boolean, aType: JdyPrimitiveType) {
        super(aInternalName, aExternalName, isKey, isNotNull);
        this.type = aType;
        this.primitive = true;
    }

    public getType (): JdyPrimitiveType {
        return this.type;
    }
}

export enum JdyDataType {

    JDY_BOOLEAN = 'BOOLEAN',
    JDY_DECIMAL = 'DECIMAL',
    JDY_TIMESTAMP = 'TIMESTAMP',
    JDY_FLOAT = 'FLOAT',
    JDY_LONG = 'LONG',
    JDY_TEXT = 'TEXT',
    JDY_VARCHAR = 'VARCHAR',
    JDY_BLOB = 'BLOB',
}

export interface JdyPrimitiveTypeVisitor {

    handleBoolean (aType: JdyBooleanType): any;

    handleDecimal (aType: JdyDecimalType): any;

    handleTimeStamp (aType: JdyTimeStampType): any;

    handleFloat (aType: JdyFloatType): any;

    handleLong (aType: JdyLongType): any;

    handleText (aType: JdyTextType): any;

    handleVarChar (aType: JdyVarCharType): any;

    handleBlob (aType: JdyBlobType): any;
}

export abstract class JdyPrimitiveType {

    private $type: JdyDataType;

    public constructor ($type: JdyDataType) {
        this.$type = $type;
    }

    abstract handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor);

    public getType (): JdyDataType {
        return this.$type;
    }
}

export class JdyBlobType extends JdyPrimitiveType {

    private typeHint: string;

    public constructor (typeHint: string) {
        super(JdyDataType.JDY_BLOB);
        this.typeHint = typeHint;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleBlob(this);
    }
}

export class JdyFloatType extends JdyPrimitiveType {

    public constructor () {
        super(JdyDataType.JDY_FLOAT);
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleFloat(this);
    }
}

export class JdyBooleanType extends JdyPrimitiveType {

    public constructor () {
        super(JdyDataType.JDY_BOOLEAN);
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleBoolean(this);
    }
}

export class JdyLongType extends JdyPrimitiveType {

    private minValue: number;
    private maxValue: number;
    private domainValues: any;

    public constructor (aMinValue: number, aMaxValue: number, aDomainValueList) {
        super(JdyDataType.JDY_LONG);
        this.minValue = aMinValue;
        this.maxValue = aMaxValue;
        this.domainValues = aDomainValueList;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleLong(this);
    }

    public getMinValue () {
        return this.minValue;
    }

    public getMaxValue () {
        return this.maxValue;
    }

    public getDomainValues (): string[] | null | undefined {
        return this.domainValues;
    }
}

export class JdyDecimalType extends JdyPrimitiveType {

    private minValue: number;
    private maxValue: number;
    private scale: number;
    private domainValues: any;

    public constructor (aMinValue: number, aMaxValue: number, aScale: number, aDomainValueList) {
        super(JdyDataType.JDY_DECIMAL);
        this.minValue = aMinValue;
        this.maxValue = aMaxValue;
        this.scale = aScale;
        this.domainValues = aDomainValueList;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleDecimal(this);
    }

    public getMinValue () {
        return this.minValue;
    }

    public getMaxValue () {
        return this.maxValue;
    }

    public getScale () {
        return this.scale;
    }

    public getDomainValues (): string[] | null | undefined {
        return this.domainValues;
    }
}

export class JdyTextType extends JdyPrimitiveType {
    private length: number;
    private typeHint: JdyTextTypeHint | null;
    private domainValues: string[] | null | undefined;

    public constructor (aLength: number, aTypeHint: JdyTextTypeHint | null, aDomainValueList?: string[] | null) {
        super(JdyDataType.JDY_TEXT);
        this.length = aLength;
        this.typeHint = aTypeHint;
        this.domainValues = aDomainValueList;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleText(this);
    }

    public getLength (): number {
        return this.length;
    }

    public getTypeHint (): JdyTextTypeHint | null {
        return this.typeHint;
    }

    public getDomainValues (): string[] | null | undefined {
        return this.domainValues;
    }
}

export enum JdyTextTypeHint {

    EMAIL = 'EMAIL',
    TELEPHONE = 'TELEPHONE',
    URL = 'URL',
}

export class JdyTimeStampType extends JdyPrimitiveType {

    private datePartUsed: boolean;
    private timePartUsed: boolean;

    public constructor (isDatePartUsed: boolean, isTimePartUsed: boolean) {
        super(JdyDataType.JDY_TIMESTAMP);
        this.datePartUsed = isDatePartUsed;
        this.timePartUsed = isTimePartUsed;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleTimeStamp(this);
    }
}

export class JdyVarCharType extends JdyPrimitiveType {

    private length: number;
    private mimeType: string | null;
    private clob: boolean;

    public constructor (aLength: number, mimeType: string, isClobFlag: boolean) {
        super(JdyDataType.JDY_VARCHAR);
        this.length = aLength;
        this.mimeType = null;
        this.clob = isClobFlag;
    }

    public handlePrimitiveKey (aHandler: JdyPrimitiveTypeVisitor) {
        return aHandler.handleVarChar(this);
    }

    public getLength (): number {
        return this.length;
    }

    getMimeType (): string | null {
        return this.mimeType;
    }
}

export class JdyAttributePath {

    private _lastInfo: JdyPrimitiveAttributeInfo;
    private _path: JdyObjectReferenceInfo[];


    constructor(path: JdyObjectReferenceInfo[], lastInfo: JdyPrimitiveAttributeInfo) {
        this._lastInfo = lastInfo;
        this._path = path;
    }

    get lastInfo(): JdyPrimitiveAttributeInfo {
        return this._lastInfo;
    }

    get path(): JdyObjectReferenceInfo[] {
        return this._path;
    }
}


export class JdyClassInfoQuery {

    private resultInfo: JdyClassInfo;
    private filterExpression: ObjectFilterExpression | null;
    public selectAttributes: JdyAttributePath[];

    public constructor (aTypeInfo: JdyClassInfo, aFilterExpr: ObjectFilterExpression | null, allSelectAttributes: JdyAttributePath[]) {
        this.resultInfo = aTypeInfo;
        this.filterExpression = aFilterExpr;
        this.selectAttributes = allSelectAttributes;
    }

    public getResultInfo (): JdyClassInfo {
        return this.resultInfo;
    }

    public matchesObject (aModel) {
        return this.filterExpression === null || this.filterExpression.matchesObject(aModel);
    }

    public getFilterExpression () {
        return this.filterExpression;
    }

    public setFilterExpression (aExpression) {
        this.filterExpression = aExpression;
    }

    public get entity (): string {
        return this.resultInfo.getInternalName();
    }
}

export class ObjectFilterExpression {

    private $exprType: string;

    public constructor (aType: string) {
        this.$exprType = aType;
    }

    public matchesObject (aModel): boolean {
        return true;
    }

    public visit (aModel): void {
    }
}

export class JdyAndExpression extends ObjectFilterExpression {

    public expressionVect: ObjectFilterExpression[];

    public constructor (aExprVect: ObjectFilterExpression[]) {
        super('AndExpression');
        this.expressionVect = aExprVect;
    }

    public visit (aVisitor) {
        return aVisitor.visitAndExpression(this);
    }

    public matchesObject (aModel) {
        let matches = true;
        for (let i = 0; i < this.expressionVect.length; i++) {
            matches = this.expressionVect[i].matchesObject(aModel);
        }
        return matches;
    }
}

export class JdyOrExpression extends ObjectFilterExpression {

    private expressionVect: ObjectFilterExpression[];

    public constructor (aExprVect: ObjectFilterExpression[]) {
        super('OrExpression');

        this.expressionVect = aExprVect;
    }

    public visit (aVisitor) {
        return aVisitor.visitOrExpression(this);
    }

    public matchesObject (aModel) {
        let matches = true;
        for (let i = 0; i < this.expressionVect.length; i++) {
            matches = this.expressionVect[i].matchesObject(aModel);
        }
        return matches;
    }
}

export class JdyOperatorExpression extends ObjectFilterExpression {

    private myOperator: ExpressionPrimitiveOperator;
    private _attributeInfo: JdyAttributeInfo;
    private _compareValue: any;

    public constructor (aOperator: ExpressionPrimitiveOperator, aAttributeInfo: JdyAttributeInfo, aCompareValue) {
        super('OperatorExpression');
        this.myOperator = aOperator;
        this._attributeInfo = aAttributeInfo;
        this._compareValue = aCompareValue;
    }

    public visit (aVisitor) {
        return aVisitor.visitOperatorExpression(this);
    }

    public matchesObject (aModel) {
        const modelValue = aModel.getValue(this._attributeInfo);
        return this.myOperator.compareValues(modelValue, this._compareValue, this._attributeInfo);
    }

    public getOperator () {
        return this.myOperator;
    }

    get compareValue() {
        return this._compareValue;
    }

    get attributeInfo (): JdyAttributeInfo {
        return this._attributeInfo;
    }
}

export class ExpressionPrimitiveOperator {

    public visitOperatorHandler (aVisitor) {
        return aVisitor.visitEqualOperator(this);
    }

    public compareValues (value1, value2, attributeInfo): boolean {
        return true;
    }
}

export class JdyEqualOperator extends ExpressionPrimitiveOperator {

    private isNotEqual: boolean;

    public constructor (notEqual: boolean) {
        super();
        this.isNotEqual = notEqual;
    }

    public visitOperatorHandler (aVisitor) {
        return aVisitor.visitEqualOperator(this);
    }

    public compareValues (value1, value2, attributeInfo) {
        let result = false;
        if (value1 !== null && value2 !== null) {
            result = attributeInfo.compareObjects(value1, value2) === 0;
            if (this.isNotEqual) {
                result = !result;
            }
        }
        return result;
    }

    public toString () {
        return (this.isNotEqual) ? '<>' : '=';
    }
}

export class JdyGreatorOperator extends ExpressionPrimitiveOperator {

    private isAlsoEqual: boolean;

    public constructor (alsoEqual: boolean) {
        super();
        this.isAlsoEqual = alsoEqual;
    }

    public visitOperatorHandler (aVisitor) {
        return aVisitor.visitGreatorOperator(this);
    }

    public compareValues (value1, value2, attributeInfo) {
        let result = false;
        if (value1 !== null && value2 !== null) {
            result = attributeInfo.compareObjects(value1, value2) > 0;
            if (this.isAlsoEqual) {
                result = result && attributeInfo.compareObjects(value1, value2) === 0;
            }
        }
        return result;
    }

    public toString () {
        return (this.isAlsoEqual) ? '>=' : '>';
    }

}

export class JdyLessOperator extends ExpressionPrimitiveOperator {

    private isAlsoEqual: boolean;

    public constructor (alsoEqual: boolean) {
        super();
        this.isAlsoEqual = alsoEqual;
    }

    public visitOperatorHandler (aVisitor) {
        return aVisitor.visitLessOperator(this);
    }

    public compareValues (value1, value2, attributeInfo) {
        let result = false;
        if (value1 !== null && value2 !== null) {
            result = attributeInfo.compareObjects(value1, value2) < 0;
            if (this.isAlsoEqual) {
                result = result && attributeInfo.compareObjects(value1, value2) === 0;
            }
        }
        return result;
    }

    public toString () {
        return (this.isAlsoEqual) ? '<=' : '<';
    }
}

export class JdyFilterCreationException extends Error {

    public constructor (m: string) {
        super(m);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, JdyValidationError.prototype);
    }
}

export class JdyPersistentException extends Error {

    public constructor (m: string) {
        super(m);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, JdyValidationError.prototype);
    }
}

export class JdyQueryCreator {

    protected resultInfo: JdyClassInfo;
    private createdExpr: ObjectFilterExpression | null;
    private attributePaths: JdyAttributePath[];

    public constructor (aResultInfo: JdyClassInfo) {
        this.resultInfo = aResultInfo;
        this.createdExpr = null;
        this.attributePaths = [];
    }

    public select( ... allAttrNames: string[]) {

        if (allAttrNames.length == 0) {
            throw new JdyFilterCreationException("There has to be at least one attribute in the select");
        } else {
            const path: JdyObjectReferenceInfo[] = [];
            let currentInfo: JdyClassInfo = this.resultInfo;
            for( let i=0; i <= allAttrNames.length-2; i++) {

                const info: JdyAttributeInfo = currentInfo.getAttr(allAttrNames[i]);
                if(info == null || !(info instanceof JdyObjectReferenceInfo)) {
                    const errorAttr = allAttrNames[i];
                    throw new JdyFilterCreationException( `Attribute ${errorAttr} has to be an ObjectReference` );
                } else {
                    const reference = info as JdyObjectReferenceInfo;
                    currentInfo = reference.getReferencedClass();
                    path.push(reference);
                }
            }
            const lastInfo: JdyAttributeInfo = currentInfo.getAttr(allAttrNames[allAttrNames.length-1]);
            if(lastInfo == null || !(lastInfo instanceof JdyPrimitiveAttributeInfo)) {
                const errorAttr = allAttrNames[allAttrNames.length-1];
                throw new JdyFilterCreationException( `Last Attribute  ${errorAttr}  has to be an Primitive`);
            }
            this.attributePaths.push(new JdyAttributePath(path, lastInfo as JdyPrimitiveAttributeInfo));
        }
        return this;
    }

    public query (): JdyClassInfoQuery {
        return new JdyClassInfoQuery(this.resultInfo, this.createdExpr, this.attributePaths);
    }

    public greater (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyGreatorOperator(false));
        return this;
    }

    public greaterOrEqual (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyGreatorOperator(true));
        return this;
    }

    public less (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyLessOperator(false));
        return this;
    }

    public lessOrEqual (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyLessOperator(true));
        return this;
    }

    public equal (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyEqualOperator(false));
        return this;
    }

    public notEqual (anExAttrName, aCompareValue) {
        this.addOperatorExpression(anExAttrName, aCompareValue, new JdyEqualOperator(true));
        return this;
    }

    public addOperatorExpression (anAttrName: string, aCompareValue, aOperator: ExpressionPrimitiveOperator) {
        const attributeInfo: JdyAttributeInfo = this.resultInfo.getAttr(anAttrName);
        if (!attributeInfo) {
            throw new JdyFilterCreationException('No attribute for attribute name in Operator Expression exists');
        }
        const opExpr = new JdyOperatorExpression(aOperator, attributeInfo, aCompareValue);
        this.addExpression(opExpr);
        return this;
    }

    public addExpression (anExpr: ObjectFilterExpression): JdyQueryCreator {
        this.createdExpr = anExpr;
        return this;
    }

    public and () {
        return new JdyAndQueryCreator(this.resultInfo, this);
    }

    public or () {
        return new JdyOrQueryCreator(this.resultInfo, this);
    }

    public end (): JdyQueryCreator {
        throw new JdyFilterCreationException('No Multiple Expression open');
    }
}

export class JdyAndQueryCreator extends JdyQueryCreator {

    private parentCreator: JdyQueryCreator;
    private expressions: ObjectFilterExpression[];


    public constructor (aResultInfo: JdyClassInfo, aParentCreator: JdyQueryCreator) {
        super(aResultInfo);
        // JdyAttributeInfo.apply(this, arguments);
        this.parentCreator = aParentCreator;
        this.expressions = [];
    }

    public addExpression (anExpr: ObjectFilterExpression): JdyQueryCreator {
        this.expressions.push(anExpr);
        return this;
    }

    public query (): JdyClassInfoQuery {
        throw new JdyFilterCreationException('And not closes');
    }

    public end () {
        this.parentCreator.addExpression(new JdyAndExpression(this.expressions));
        return this.parentCreator;
    }
}

export class JdyOrQueryCreator extends JdyQueryCreator {

    private parentCreator: JdyQueryCreator;
    private expressions: ObjectFilterExpression[];

    public constructor (aResultInfo, aParentCreator) {
        super(aResultInfo);
        // JdyAttributeInfo.apply(this, arguments);
        this.parentCreator = aParentCreator;
        this.expressions = [];
    }

    public addExpression (anExpr): JdyQueryCreator {
        this.expressions.push(anExpr);
        return this;
    }

    public query (): JdyClassInfoQuery {
        throw new JdyFilterCreationException('Or not closes');
    }

    public end () {
        this.parentCreator.addExpression(new JdyOrExpression(this.expressions));
        return this.parentCreator;
    }
}

export interface JdyObjectList {

    done (anCallback);

    add (anValueObject);
}

export class JdyObjectListImpl implements JdyObjectList {

    private assocObj: JdyAssociationModel;
    private objects: any[] = [];

    public constructor (anAssocInfo: JdyAssociationModel) {

        this.assocObj = anAssocInfo;
    }

    public done (anCallback) {
        anCallback(this.objects);
    }

    public add (anValueObject) {
        this.objects.push(anValueObject);
    }

    public getObjects (): any[] {

        return this.objects;
    }
}

export class JdyProxyObjectList implements JdyObjectList {

    private objects: any[] = [];
    private assocObj: JdyAssociationModel;
    private proxyResolver;
    private masterObject: JdyTypedValueObject;
    private promise: null | any;

    public constructor (anAssocInfo: JdyAssociationModel, aProxyResolver: any, aMasterObj: JdyTypedValueObject) {

        this.assocObj = anAssocInfo;
        this.masterObject = aMasterObj;
        this.proxyResolver = aProxyResolver;
    }

    public done (anCallback) {
        let dfrd;
        const that = this;

        if (!this.promise) {

            this.promise = new Promise((resolve, reject) => {
                resolve(123);
            });

            if (this.proxyResolver) {

                this.proxyResolver.resolveAssoc(that.assocObj, that.masterObject, aAssocList => {
                    this.objects = aAssocList;
                    this.promise.resolve(aAssocList);
                });
            } else {
                this.promise.reject('Proxy Error: no proxy resolver ' + this.assocObj.getAssocName());
            }
        }

        this.promise.done(anCallback);
    }

    public add (anValueObject) {
        this.objects.push(anValueObject);
    }

}

export class JdyTypedValueObject {

    public $typeInfo: JdyClassInfo;
    public $assocs: { [name: string]: JdyObjectList } = {};
    private $proxyResolver;
    private $proxy: any = null;

    public constructor (aClassInfo: JdyClassInfo, aProxyResolver: any | null, asProxy: boolean) {

        this.$typeInfo = aClassInfo;
        this.$proxyResolver = aProxyResolver;

        if (asProxy) {
            this.$proxy = {};
        }

        aClassInfo.forEachAttr(
            curAttrInfo => {
                this[curAttrInfo.getInternalName()] = null;
            }
        );

        aClassInfo.forEachAssoc(
            curAssocInfo => {
                if (aProxyResolver) {
                    this.$assocs[curAssocInfo.getAssocName()] = new JdyProxyObjectList(curAssocInfo, aProxyResolver, this);
                } else {
                    this.$assocs[curAssocInfo.getAssocName()] = new JdyObjectListImpl(curAssocInfo);
                }
            }
        );
    }

    public copy (): JdyTypedValueObject {

        const objectCopy: JdyTypedValueObject = new JdyTypedValueObject(this.$typeInfo, this.$proxyResolver, this.$proxy);

        this.$typeInfo.forEachAttr(
            curAttrInfo => {
                objectCopy[curAttrInfo.getInternalName()] = this.val(curAttrInfo);
            }
        );

        this.$typeInfo.forEachAssoc(
            curAssocInfo => {
                objectCopy.$assocs[curAssocInfo.getAssocName()] = this.assocVals(curAssocInfo);
            }
        );

        return objectCopy;
    }

    public setAssocVals (anAssoc, anObjectList: JdyObjectList) {

        const assocName = (typeof anAssoc === 'string') ? anAssoc : anAssoc.getAssocName();
        this.$assocs[assocName] = anObjectList;
    }

    public assocVals (anAssoc): JdyObjectList {

        const assocName = (typeof anAssoc === 'string') ? anAssoc : anAssoc.getAssocName();
        const assocObj = this.$assocs[assocName];
        return assocObj;
    }

    public val (anAttr) {

        return this[(typeof anAttr === 'string') ? anAttr : anAttr.getInternalName()];
    }

    public setVal (anAttr, aValue) {

        if (typeof anAttr === 'string') {
            this[anAttr] = aValue;
        } else {
            this[anAttr.getInternalName()] = aValue;
        }
    }

    public setProxyVal (anAttr, aValue) {

        this.$proxy[anAttr.getInternalName()] = aValue;
    }

}
