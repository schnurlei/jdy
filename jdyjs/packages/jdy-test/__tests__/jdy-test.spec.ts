import {
    JdyClassInfo,
    JdyClassInfoQuery,
    JdyQueryCreator,
    JdyRepository,
    JdyValidationError
} from '@jdynameta/jdy-base';
import { testCreatePlantShopRepository } from '../src//jdy-test';

test(' Filter creation', () => {

    let query: JdyClassInfoQuery;
    let rep = testCreatePlantShopRepository();
    let plantType = rep.getClassInfo('Plant');

    expect(plantType).not.toBeNull();
    if (plantType !== null) {
        query = new JdyQueryCreator(plantType)
            .or()
            .equal('botanicName', 'Iris')
            .and().greater('heigthInCm', 30).less('heigthInCm', 100).end()
            .end().query();
        expect(query.getResultInfo().getInternalName()).toBe('Plant');
    }
});
