import { JdyRepository } from '@jdynameta/jdy-base';

export const testCreatePlantShopRepository = function (): JdyRepository {

    let rep = new JdyRepository('TestApp');

    let plantType = rep.addClassInfo('Plant', null);
    plantType.addTextAttr('botanicName', 200).setIsKey(true).setNotNull(true);
    plantType.addLongAttr('heigthInCm', 0, 5000);
    plantType.addTextAttr('plantFamily', 100).setGenerated(true);
    plantType.addTextAttr('color', 100, ['blue', 'red', 'green']);
    plantType.addBooleanAttr('wintergreen');
    plantType.addDecimalAttr('price', 0.0, 200.0, 2);
    plantType.addTimeStampAttr('lastChanged', true, true);
    plantType.addVarCharAttr('description', 1000);

    let orderItemType = rep.addClassInfo('OrderItem', null).setStandalone(false);
    orderItemType.addLongAttr('ItemNr', 0, 1000).setIsKey(true);
    orderItemType.addLongAttr('ItemCount', 0, 1000).setNotNull(true);
    orderItemType.addDecimalAttr('Price', 0.0000, 1000.0000, 4).setNotNull(true);
    orderItemType.addReference('Plant', plantType).setIsDependent(false).setNotNull(true);

    let bankAccountType = rep.addClassInfo('BankAccount', null).setStandalone(false);
    bankAccountType.addTextAttr('IBAN', 24).setIsKey(true);
    bankAccountType.addTextAttr('BIC', 11);
    bankAccountType.addTextAttr('Bank', 100);

    let addressType = rep.addClassInfo('Address', null).setStandalone(false);
    addressType.addTextAttr('AddressId', 30).setIsKey(true);
    addressType.addTextAttr('Street', 30).setNotNull(true);
    addressType.addTextAttr('ZipCode', 30).setNotNull(true);
    addressType.addTextAttr('City', 30).setNotNull(true);
    addressType.addEmailAttr('EMail', 30, null).setNotNull(true);

    let customerType = rep.addClassInfo('Customer', null);
    customerType.addTextAttr('CustomerId', 30).setIsKey(true);
    customerType.addTextAttr('FirstName', 30).setNotNull(true);
    customerType.addTextAttr('LastName', 30).setNotNull(true);
    customerType.addReference('PrivateAddress', addressType).setIsDependent(true).setNotNull(true);
    customerType.addReference('InvoiceAddress', addressType).setIsDependent(true).setNotNull(false);

    let orderType = rep.addClassInfo('PlantOrder', null);
    orderType.addLongAttr('OrderNr', 0, 999999999).setIsKey(true);
    orderType.addTimeStampAttr('OrderDate', true, false).setNotNull(true);

    rep.addAssociation('Items', orderType, orderItemType, 'Order', 'Order'
        , false, false, false);
    rep.addAssociation('Accounts', orderType, bankAccountType, 'Order', 'Order'
        , false, false, false);

    return rep;
};

export const plantShopMessages = {
    en: {
        TestApp: {
            Plant: {
                botanicName: 'Botanic Name',
                heigthInCm: 'Heigth [cm]',
                plantFamily: 'Plant Family',
                color: 'Color',
                wintergreen: 'Wintergreen',
                price: 'Price',
                lastChanged: 'Last Changed',
                description: 'Description'
            }
        }
    },
    de: {
        TestApp: {
            Plant: {
                botanicName: 'Botanischer Name',
                heigthInCm: 'Höhe [cm]',
                plantFamily: 'Pflanzenfamilie',
                color: 'Fabe',
                wintergreen: 'wintergrün',
                price: 'Preis',
                lastChanged: 'Letzte Änderung',
                description: 'Beschreibung'
            }
        }
    }
};
