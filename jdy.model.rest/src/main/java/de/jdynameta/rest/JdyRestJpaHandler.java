package de.jdynameta.rest;

import de.jdynameta.base.VersionInformation;
import de.jdynameta.base.metainfo.*;
import de.jdynameta.base.metainfo.filter.ClassInfoQuery;
import de.jdynameta.base.metainfo.primitive.*;
import de.jdynameta.base.objectlist.DefaultObjectList;
import de.jdynameta.base.objectlist.ObjectList;
import de.jdynameta.base.value.JdyPersistentException;
import de.jdynameta.base.value.TypedValueObject;
import de.jdynameta.base.value.ValueObject;
import de.jdynameta.jdy.model.jpa.*;
import de.jdynameta.json.JsonCompactFileReader;
import de.jdynameta.json.JsonFileReader;
import de.jdynameta.json.JsonFileWriter;
import de.jdynameta.metamodel.application.AppRepository;
import de.jdynameta.metamodel.application.ApplicationRepository;
import de.jdynameta.metamodel.application.MetaRepositoryCreator;
import de.jdynameta.metamodel.filter.FilterCreator;
import de.jdynameta.metamodel.filter.FilterRepository;
import de.jdynameta.persistence.manager.PersistentOperation;
import de.jdynameta.persistence.state.ApplicationObj;
import de.jdynameta.persistence.state.ApplicationObjImpl;


import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.xml.transform.TransformerConfigurationException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class JdyRestJpaHandler {

    private static final Logger LOG = Logger.getLogger(JdyRestJpaHandler.class.getName());
    private EntityManager entityManager;

    public JdyRestJpaHandler(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public String handleMetaDataReqeust() {

        ClassRepository metaRepo = convertMetamodel2Repository(this.entityManager.getMetamodel());

        try {
            AppRepository metaRepoVo = convertMetaRepo2MetaRepoVo(metaRepo);
            ClassInfo metaRepoInfo = ApplicationRepository.getSingleton().getClassForName("AppRepository");

            return valueObjectToJsonString(metaRepoVo, metaRepoInfo);
        } catch (JdyPersistentException | TransformerConfigurationException ex) {
            ex.printStackTrace();
            throw new GeneralRestException(ex);
        }
    }

    private String valueObjectToJsonString(AppRepository metaRepoVo, ClassInfo metaRepoInfo) throws TransformerConfigurationException, JdyPersistentException {

        DefaultObjectList<TypedValueObject> singleElementList = new DefaultObjectList<>(metaRepoVo);
        JsonFileWriter jsonFileWriter = new JsonFileWriter(new JsonFileWriter.WriteAllDependentStrategy(), true);
        StringWriter writer = new StringWriter();
        jsonFileWriter.writeObjectList(writer, metaRepoInfo,singleElementList, PersistentOperation.Operation.READ);
        return writer.toString();
    }

    private ClassRepository convertMetamodel2Repository(Metamodel metamodel) {

        JpaMetamodelReader reader = new JpaMetamodelReader();
        return reader.createMetaRepository(metamodel, "TestApp");
    }

    private AppRepository convertMetaRepo2MetaRepoVo(ClassRepository repo) throws JdyPersistentException {

        return new MetaRepositoryCreator(null).createAppRepository(repo);
    }


    public String handleGetDataRequest(String className, String filter) {
        final ClassInfo entityClassInfo = this.getMetaInfoClassForClassName(className);
        final Optional<EntityType<?>> jpaEntityForName = this.getJpaEntityTypeForClassName(className);

        if( entityClassInfo != null && jpaEntityForName.isPresent() ) {

            final List<TypedValueObject> allDbObjects;
            try {
                if(filter != null) {
                    allDbObjects =  this.readObjectsFromDbForFilter(filter);
                } else {
                    List<?> readJpaObjects = readAllObjectsFromDbForEntity(jpaEntityForName.get());
                    allDbObjects = convertJpaObjectsToValueObjects(readJpaObjects, entityClassInfo);
                }

                return this.createJsonResponse(entityClassInfo, allDbObjects);
            } catch(JdyPersistentException ex) {
                throw new GeneralRestException(ex);
            }
        } else {
            return "";
        }
    }


    public void handleDeleteRequest(String className, Map<String, String> parameters) {
        final ClassInfo entityClassInfo = this.getMetaInfoClassForClassName(className);
        final Optional<EntityType<?>> jpaEntityForName = this.getJpaEntityTypeForClassName(className);

        if( entityClassInfo != null && jpaEntityForName.isPresent() ) {

            try {
                JpaWriter writer = new JpaWriter();

                final ApplicationObj objToDelete = convertParameters(parameters, entityClassInfo);
                writer.deleleteInDb(objToDelete, jpaEntityForName.get(), this.entityManager);

                return;

            } catch (JdyPersistentException  ex) {
                throw new GeneralRestException(ex);
            }

        } else {
            throw new GeneralRestException("Invalid entity type ");
        }
    }

    public String handleInsertRequest(String className, String jsonEntity) {

        final ClassInfo entityClassInfo = this.getMetaInfoClassForClassName(className);
        final Optional<EntityType<?>> jpaEntityForName = this.getJpaEntityTypeForClassName(className);

        if( entityClassInfo != null && jpaEntityForName.isPresent() ) {

            try(StringReader reader = new StringReader(jsonEntity)) {

                JsonFileReader jsonReader = new JsonFileReader();
                ObjectList<ApplicationObj> objectList = jsonReader.readObjectList(reader,entityClassInfo);
                JpaWriter writer = new JpaWriter();
                TypedValueObject insertedObject = writer.insertInDb(objectList.get(0), jpaEntityForName.get(), this.entityManager);
                DefaultObjectList<TypedValueObject> singleElementList = new DefaultObjectList<>(insertedObject);
                JsonFileWriter jsonFileWriter = new JsonFileWriter(new JsonFileWriter.WriteAllDependentStrategy(), true);
                final StringWriter resultWriter = new StringWriter();
                jsonFileWriter.writeObjectList(resultWriter,entityClassInfo,singleElementList, PersistentOperation.Operation.READ);

                return resultWriter.toString();

            } catch (JdyPersistentException | TransformerConfigurationException ex) {
                throw new GeneralRestException(ex);
            }

        } else {
            throw new GeneralRestException("Invalid entity type ");
        }
    }

    public String handleUpdateRequest(String className, String jsonEntity) {

        final ClassInfo entityClassInfo = this.getMetaInfoClassForClassName(className);
        final Optional<EntityType<?>> jpaEntityForName = this.getJpaEntityTypeForClassName(className);

        if( entityClassInfo != null && jpaEntityForName.isPresent() ) {

            try(StringReader reader = new StringReader(jsonEntity)) {

                JsonFileReader jsonReader = new JsonFileReader();
                ObjectList<ApplicationObj> objectList = jsonReader.readObjectList(reader,entityClassInfo);
                JpaWriter writer = new JpaWriter();
                TypedValueObject insertedObject = writer.insertInDb(objectList.get(0), jpaEntityForName.get(), this.entityManager);
                DefaultObjectList<TypedValueObject> singleElementList = new DefaultObjectList<>(insertedObject);
                JsonFileWriter jsonFileWriter = new JsonFileWriter(new JsonFileWriter.WriteAllDependentStrategy(), true);
                final StringWriter resultWriter = new StringWriter();
                jsonFileWriter.writeObjectList(resultWriter,entityClassInfo,singleElementList, PersistentOperation.Operation.READ);

                return resultWriter.toString();

            } catch (JdyPersistentException | TransformerConfigurationException ex) {
                throw new GeneralRestException(ex);
            }

        } else {
            throw new GeneralRestException("Invalid entity type ");
        }
    }

    private ApplicationObj convertParameters(Map<String, String> parameters, ClassInfo entityClassInfo) throws JdyPersistentException {

        RequestParameterHandler handler = new RequestParameterHandler(parameters, entityClassInfo, null, false);
        entityClassInfo.handleAttributes(handler, null);
        return  handler.result;

    }

    private ClassInfo getMetaInfoClassForClassName(final String className) {

        final ClassRepository metaRepo = new JpaMetamodelReader().createMetaRepository(this.entityManager.getMetamodel(), "TestApp");
        return metaRepo.getClassForName(className);
    }

    private Optional<EntityType<?>> getJpaEntityTypeForClassName(final String className) {

        return this.entityManager.getMetamodel().getEntities()
                .stream()
                .filter(entity -> entity.getName().equals(className)).findFirst();
    }

    private List<?> readAllObjectsFromDbForEntity(final EntityType<?> entityType) {

        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<?> query = criteriaBuilder.createQuery(entityType.getJavaType());
        query.from(entityType.getJavaType());

        return this.entityManager.createQuery(query).getResultList();
    }

    private List<TypedValueObject> convertJpaObjectsToValueObjects(List<?> readedJpaObjects, final ClassInfo entityClassInfo) {

        final List<TypedValueObject> wrappedEnitites = readedJpaObjects.stream()
                .map(entity-> new TypedReflectionValueObjectWrapper(entity, entityClassInfo))
                .collect(Collectors.toList());
        return wrappedEnitites;
    }

    private List<TypedValueObject> readObjectsFromDbForFilter(final String filter) throws JdyPersistentException {

        final TypedValueObject appQuery = convertFilterStringToAppQuery(filter);
        final ClassInfoQuery jdyQuery = convertAppQueryToMetaQuery(appQuery);
        if(jdyQuery.selectAttributes() == null || jdyQuery.selectAttributes().isEmpty()) {
            final CriteriaQuery<Object> criteriaQuery = convertMetaQueryToJpaCriteria(jdyQuery);
            List<?> readedJpaObjects = this.entityManager.createQuery(criteriaQuery).getResultList();
            return convertJpaObjectsToValueObjects(readedJpaObjects, jdyQuery.getResultInfo());
        } else {
            final CriteriaQuery<Tuple> criteriaQuery = convertMetaQueryToJpaCriteriaWithSelect(jdyQuery);
            List<Tuple> readedObjects = this.entityManager.createQuery(criteriaQuery).getResultList();
            final List<TypedValueObject> wrappedEnitites = readedObjects.stream()
                    .map(entity-> new JpaTupleConverter().convertTuppleToValueObject(entity,jdyQuery.selectAttributes(), jdyQuery.getResultInfo()))
                    .collect(Collectors.toList());
            return wrappedEnitites;
        }
    }

    private ApplicationObj convertFilterStringToAppQuery(final String filter) throws JdyPersistentException {

        JsonCompactFileReader reader = new JsonCompactFileReader(this.getMapping() , FilterRepository.getSingleton().getRepoName(), null );
        ObjectList<ApplicationObj> appQuery = reader.readObjectList(new StringReader(filter), FilterRepository.getSingleton().getInfoForType(FilterRepository.TypeName.AppQuery));
        return appQuery.get(0);
    }

    private CriteriaQuery<Object>  convertMetaQueryToJpaCriteria(ClassInfoQuery newQuery) throws JdyPersistentException {

        final JpaFilterConverter filterConverter = new JpaFilterConverter(this.entityManager);
        final CriteriaQuery<Object> criteriaQuery = filterConverter.convert(newQuery);
        return criteriaQuery;
    }

    private ClassInfoQuery convertAppQueryToMetaQuery(TypedValueObject appQuery ) throws JdyPersistentException {

        final ClassRepository metaRepo = new JpaMetamodelReader().createMetaRepository(this.entityManager.getMetamodel(), "TestApp");
        final ClassInfoQuery newQuery = new FilterCreator().createMetaFilter(appQuery, metaRepo);
        return newQuery;
    }

    private CriteriaQuery<Tuple>  convertMetaQueryToJpaCriteriaWithSelect(ClassInfoQuery newQuery) throws JdyPersistentException {

        final JpaFilterConverter filterConverter = new JpaFilterConverter(this.entityManager);
        final CriteriaQuery<Tuple> criteriaQuery = filterConverter.convertWithSelect(newQuery);
        return criteriaQuery;
    }

    private List<?> readObjectsFromDbForEntity(final EntityType<?> entityType, final String filter) throws JdyPersistentException {

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<?> query = criteriaBuilder.createQuery(entityType.getJavaType());
        query.from(entityType.getJavaType());

        JsonCompactFileReader reader = new JsonCompactFileReader(this.getMapping() , FilterRepository.getSingleton().getRepoName(), null );
        ObjectList<ApplicationObj> appQuery = reader.readObjectList(new StringReader(filter), FilterRepository.getSingleton().getInfoForType(FilterRepository.TypeName.AppQuery));

        final ClassRepository metaRepo = new JpaMetamodelReader().createMetaRepository(this.entityManager.getMetamodel(), "TestApp");

        final FilterCreator creator = new FilterCreator();
        final ClassInfoQuery newQuery = creator.createMetaFilter(appQuery.get(0), metaRepo);

        final JpaFilterConverter filterConverter = new JpaFilterConverter(this.entityManager);
        final CriteriaQuery<Object> criteriaQuery = filterConverter.convert(newQuery);

        return this.entityManager.createQuery(criteriaQuery).getResultList();
    }

    private HashMap<String, String> getMapping()
    {
        final HashMap<String, String> att2AbbrMap = new HashMap<>();
        att2AbbrMap.put("repoName", "rn");
        att2AbbrMap.put("className", "cn");
        att2AbbrMap.put("expr", "ex");
        att2AbbrMap.put("orSubExpr", "ose");
        att2AbbrMap.put("andSubExpr", "ase");
        att2AbbrMap.put("attrName", "an");
        att2AbbrMap.put("operator", "op");
        att2AbbrMap.put("isNotEqual", "ne");
        att2AbbrMap.put("isAlsoEqual", "ae");
        att2AbbrMap.put("longVal", "lv");
        att2AbbrMap.put("textVal", "tv");
        return att2AbbrMap;
    }

    private String createJsonResponse(final ClassInfo entityClassInfo, List<TypedValueObject> wrappedEnitites) {

        final JsonFileWriter jsonFileWriter = new JsonFileWriter(new JsonFileWriter.WriteAllDependentStrategy(), true);
        final StringWriter writer = new StringWriter();
        try {
            jsonFileWriter.writeObjectList(writer, entityClassInfo, new DefaultObjectList<>(wrappedEnitites), PersistentOperation.Operation.READ);
            return writer.toString();
        } catch (JdyPersistentException | TransformerConfigurationException ex) {
            LOG.log(Level.SEVERE, "Error creating JSON Response", ex);
            throw new GeneralRestException(ex);
        }
    }

    public static class RequestParameterHandler implements AttributeHandler
    {
        private final Map<String, String> paraMap;
        private final ApplicationObj result;
        private final List<AttributeInfo> aspectPath;
        private final boolean parentisKey;

        public RequestParameterHandler(final Map<String, String> aRequestParaMap, final ClassInfo aConcreteClass, final List<AttributeInfo> anAspectPath
                , final boolean parentisKey)
        {
            super();
            this.result = new ApplicationObjImpl(aConcreteClass, false);
            this.paraMap = aRequestParaMap;
            this.aspectPath = anAspectPath;
            this.parentisKey = parentisKey;
        }

        @Override
        public void handleObjectReference(final ObjectReferenceAttributeInfo aInfo, final ValueObject objToHandle)
                throws JdyPersistentException
        {

            if (aInfo.isKey() || parentisKey)
            {
                final List<AttributeInfo> refAspectPath = new ArrayList<>();
                if (this.aspectPath != null)
                {
                    refAspectPath.addAll(this.aspectPath);
                }
                refAspectPath.add(aInfo);
                final RequestParameterHandler refHandler = new RequestParameterHandler(this.paraMap, aInfo.getReferencedClass(), refAspectPath, true);
                aInfo.getReferencedClass().handleAttributes(refHandler, null);
                this.result.setValue(aInfo, refHandler.result);
            }
        }

        @Override
        public void handlePrimitiveAttribute(final PrimitiveAttributeInfo aInfo, final Object objToHandle)
                throws JdyPersistentException
        {
            final String parameterName = this.createParameterName(this.aspectPath, aInfo);
            if (aInfo.isKey() || parentisKey)
            {
                final String value = this.paraMap.get(parameterName);
                if (value == null || value.trim().isEmpty())
                {
                    throw new JdyPersistentException("Missing value for type in attr value: " + aInfo.getInternalName());
                } else
                {
                    this.result.setValue(aInfo, aInfo.getType().handlePrimitiveKey(new StringValueGetVisitor(value)));
                }
            }
        }

        private String createParameterName(final List<AttributeInfo> aAspectPath, final PrimitiveAttributeInfo aInfo)
        {
            final StringBuilder resultName = new StringBuilder();
            if (this.aspectPath != null)
            {
                for (final AttributeInfo attributeInfo : aAspectPath)
                {
                    resultName.append(attributeInfo.getInternalName()).append('.');
                }
            }

            resultName.append(aInfo.getInternalName());
            return resultName.toString();
        }
    }

    public static class StringValueGetVisitor implements PrimitiveTypeGetVisitor
    {
        private final String attrValue;

        /**
         *
         */
        public StringValueGetVisitor(final String anAttrValue)
        {
            super();
            this.attrValue = anAttrValue;
        }

        @Override
        public Boolean handleValue(final BooleanType aType) throws JdyPersistentException
        {
            return Boolean.valueOf(this.attrValue);
        }

        @Override
        public BigDecimal handleValue(final CurrencyType aType)
                throws JdyPersistentException
        {
            return new BigDecimal(this.attrValue);
        }

        @Override
        public Date handleValue(final TimeStampType aType) throws JdyPersistentException
        {
            try {
                final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
                final ZonedDateTime zdt = ZonedDateTime.parse(this.attrValue, formatter.withZone(ZoneId.systemDefault()));
                return Date.from(zdt.toInstant());
            } catch (final DateTimeParseException ex) {
                throw new JdyPersistentException(ex);
            }
        }

        @Override
        public Double handleValue(final FloatType aType) throws JdyPersistentException
        {
            return Double.valueOf(this.attrValue);
        }

        @Override
        public Long handleValue(final LongType aType) throws JdyPersistentException
        {
            return Long.valueOf(this.attrValue);
        }

        @Override
        public String handleValue(final TextType aType) throws JdyPersistentException
        {
            return this.attrValue;
        }

        @Override
        public String handleValue(final VarCharType aType) throws JdyPersistentException
        {
            return this.attrValue;
        }

        @Override
        public BlobByteArrayHolder handleValue(final BlobType aType) throws JdyPersistentException
        {
            // TODO Auto-generated method stub
            return null;
        }
    }

}
