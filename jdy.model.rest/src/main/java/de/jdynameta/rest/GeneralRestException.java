package de.jdynameta.rest;

public class GeneralRestException extends RuntimeException {

    public GeneralRestException() {
        super();
    }
    public GeneralRestException(String message, Throwable cause) {
        super(message, cause);
    }
    public GeneralRestException(String message) {
        super(message);
    }
    public GeneralRestException(Throwable cause) {
        super(cause);
    }
}
