package de.jdynameta.jdy.model.mongodb;

import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
class MongoDbAnalyseTest {

    @Container
    public GenericContainer redis = new GenericContainer(DockerImageName.parse("mongo:4.4.2"))
            .withExposedPorts(6379);
}