package de.jdynameta.json;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.jdynameta.base.metainfo.primitive.*;
import de.jdynameta.base.value.JdyPersistentException;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;

/**
 * Add Primitive values to a Json Object. Set #setCurPrimitivName before call o handleValue
 */
public class JsonPrimitiveHandler implements PrimitiveTypeVisitor
{
    private ObjectNode parentJsonObj;
    private String curPrimitivName;

    public JsonPrimitiveHandler()
    {
    }

    public void setCurPrimitivName(ObjectNode aParentJsonObject, String curPrimitivName)
    {
        this.parentJsonObj = aParentJsonObject;
        this.curPrimitivName = curPrimitivName;
    }



    @Override
    public void handleValue(BigDecimal aValue, CurrencyType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }

    @Override
    public void handleValue(Boolean aValue, BooleanType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }

    @Override
    public void handleValue(Date aValue, TimeStampType aType) throws JdyPersistentException
    {
        if( aValue != null ) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date date = new Date(aValue.getTime());
            ZonedDateTime zoneDateTime = ZonedDateTime.ofInstant(date.toInstant(),
                    ZoneId.systemDefault());
            String zdtString = formatter.withZone(ZoneId.systemDefault()).format(zoneDateTime.withNano(0));


            parentJsonObj.put(curPrimitivName, zdtString);
        } else {
            parentJsonObj.putNull(curPrimitivName);
        }
    }

    @Override
    public void handleValue(Double aValue, FloatType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }

    @Override
    public void handleValue(Long aValue, LongType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }

    @Override
    public void handleValue(String aValue, TextType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }

    @Override
    public void handleValue(String aValue, VarCharType aType) throws JdyPersistentException
    {
        parentJsonObj.put(curPrimitivName, aValue);
    }


@Override
    public void handleValue(byte [] aValue, BlobType aType) throws JdyPersistentException
    {
        if( aValue != null ) {
            final String encoded = Base64.getEncoder().encodeToString(aValue);
            parentJsonObj.put(curPrimitivName, encoded);
        } else {
            parentJsonObj.putNull(curPrimitivName);
        }
    }

    public static void main(String[] args) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        Date date = new Date();
        ZonedDateTime zoneDateTime = ZonedDateTime.ofInstant(date.toInstant(),
                ZoneId.of("UTC"));
        String zdtString = formatter.withZone(ZoneId.systemDefault()).format(zoneDateTime.withNano(0));

        System.out.println(zdtString);
    }

}
