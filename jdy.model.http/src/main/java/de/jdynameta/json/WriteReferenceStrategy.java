package de.jdynameta.json;

import de.jdynameta.base.metainfo.AssociationInfo;
import de.jdynameta.base.metainfo.ClassInfo;
import de.jdynameta.base.metainfo.ObjectReferenceAttributeInfo;
import de.jdynameta.base.value.TypedValueObject;

public interface WriteReferenceStrategy
{
    public boolean isWriteAsProxy(ClassInfo classInfo, ObjectReferenceAttributeInfo attrInfo, TypedValueObject refObj);

    public boolean isWriteAsProxy(ClassInfo classInfo, AssociationInfo assocInfo);
}
