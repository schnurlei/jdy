import Vue, { VNode } from 'vue';
import Dev from './serve.vue';
import { i18n } from '../src/i18n';
// To register individual components where they are used (serve.vue) instead of using the
// library as a whole, comment/remove this import and it's corresponding "Vue.use" call
import JdyVuetify from '@/entry.esm';
Vue.use(JdyVuetify);

Vue.config.productionTip = false;

new Vue({
  i18n,
  render: (h): VNode => h(Dev),
}).$mount('#app');
