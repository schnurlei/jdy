import { PluginFunction } from 'vue';
import JdyBooleanField from "./src/components/part/boolean/JdyBooleanField.vue";
import JdyDecimalField from "./src/components/part/decimal/JdyDecimalField.vue";
import JdyImageField from "./src/components/part/image/JdyImageField.vue";
import JdyLongField from "./src/components/part/long/JdyLongField.vue";
import JdyNumericTextField from "./src/components/part/numeric/JdyNumericTextField.vue";
import JdyPrimitiveComponentHolder from "./src/components/part/primitive/JdyPrimitiveComponentHolder.vue";
import JdyStringField from "./src/components/part/string/JdyStringField.vue";
import JdyTextField from "./src/components/part/text/JdyTextField.vue";
import JdyTimestampField from "./src/components/part/timestamp/JdyTimestampField.vue";
import JdyObjectReferenceField from "./src/components/part/reference/JdyObjectReferenceField.vue";
import JdyBooleanFilterField from './src/components/filter/boolean/JdyBooleanFilterField.vue'
import JdyDecimalFilterField from "./src/components/filter/decimal/JdyDecimalFilterField.vue";
import JdyExpressionForm from "./src/components/filter/expression/JdyExpressionForm.vue";
import JdyFilterPanel from "./src/components/filter/panel/JdyFilterPanel.vue";
import JdyLongFilterField from "./src/components/filter/long/JdyLongFilterField.vue";
import JdyPrimitiveFilterFieldHolder from "./src/components/filter/primitiv/JdyPrimitiveFilterFieldHolder.vue";
import JdyStringFilterField from "./src/components/filter/string/JdyStringFilterField.vue";
import JdyTimestampFilterField from "./src/components/filter/timestamp/JdyTimestampFilterField.vue";
import JdyGeneratedPanel from "./src/components/panel/JdyGeneratedPanel.vue";
import JdySimpleTable from "./src/components/panel/JdySimpleTable.vue";

declare const JdyVuetify: PluginFunction<any>;
export default JdyVuetify;

export { JdyBooleanField, JdyDecimalField, JdyImageField, JdyLongField, JdyNumericTextField
    , JdyPrimitiveComponentHolder, JdyStringField, JdyTextField, JdyTimestampField, JdyObjectReferenceField
    , JdyBooleanFilterField, JdyDecimalFilterField, JdyExpressionForm, JdyFilterPanel, JdyLongFilterField
    , JdyPrimitiveFilterFieldHolder, JdyStringFilterField, JdyTimestampFilterField
    , JdyGeneratedPanel, JdySimpleTable
}