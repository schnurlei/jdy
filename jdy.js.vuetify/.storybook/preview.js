// Imports for configuring Vuetify
import Vue from 'vue'
import Vuetify from 'vuetify'
import {i18n} from "../src/i18n.ts";
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css';

// configure Vue to use Vuetify
Vue.use(Vuetify)

// this was the only thing here by default
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

// instantiate Vuetify instance with any component/story level params
const vuetify = new Vuetify({
  icons: {
    iconfont: 'mdi'
  }
});

// THIS is my decorator
export const decorators = [
  (story, context) => {
    // wrap the passed component within the passed context
    const wrapped = story(context)
    // extend Vue to use Vuetify around the wrapped component
    return Vue.extend({
      vuetify,
      i18n,
      components: { wrapped },
      template: `
        <v-app>
          <v-container fluid>
            <wrapped />
          </v-container>
        </v-app>
      `
    })
  },
]