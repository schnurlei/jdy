# jdy.js.vuetify

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### publish
```
yarn clean
yarn tsc 
yarn login
npm publish --access=public
```

### rollup
...
npm i -g vue-sfc-rollup
...