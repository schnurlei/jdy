import { required, email, max, min_value, max_value } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("required", {
    ...required,
    message: "This field is required"
});

extend("max", {
    ...max,
    message: "This field must be {length} characters or less"
});

extend("email", {
    ...email,
    message: "This field must be a valid email"
});

extend("min_value", {
    ...min_value,
    message: "The field must be {min} or more"
});

extend("max_value", {
    ...max_value,
    message: "The field must be {max} or less"
});

// @ts-ignore
extend("decimal", {
    // @ts-ignore
    validate: (value, { decimals = '*', separator = '.' } = {}) => {
        if (value === null || value === undefined || value === '') {
            return {
                valid: false
            };
        }
        if (Number(decimals) === 0) {
            return {
                valid: /^-?\d*$/.test(value),
            };
        }
        const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`;
        const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`);

        return {
            valid: regex.test(value),
        };
    },
    message: 'The {_field_} field must contain only decimal values'
})