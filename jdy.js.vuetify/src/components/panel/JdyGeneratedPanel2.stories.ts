import { storiesOf } from '@storybook/vue';
import JdyGeneratedPanel2 from './JdyGeneratedPanel2.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../i18n";


const rep = new JdyRepository('TestRepo');

const referenzClass = rep.addClassInfo('ReferenzClass', null);
referenzClass.addLongAttr("id", 0 , 100000).setIsKey(true);
referenzClass.addTextAttr("description", 2000 , null);

const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
export const textAttr = createdClass.addTextAttr('TestTextAttr', 30, null)
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);
export const refAttr1 = createdClass.addReference('ObjectReferenceAttr1', referenzClass);
export const refAttr2 = createdClass.addReference('ObjectReferenceAttr2', referenzClass);

const messages = { 'TestRepo':
                    {'TestClass':
                            {
                                'TestBooleanAttr': "TestBooleanEn",
                                'TestDecimalAttr': "TestDecimalAttrEn",
                                'TestTextAttr': "TestTextAttrEn",
                                'TestTimestampAttr': "TestTimestampAttrEn",
                                'ObjectReferenceAttr1': "ObjectReferenceAttr1En",
                                'ObjectReferenceAttr2': "ObjectReferenceAttr2En"
                            }
                    }
                };
i18n.mergeLocaleMessage('en', messages);
i18n.mergeLocaleMessage('de', messages);

const editItem = { TestBooleanAttr: true,
    TestDecimalAttr: 22.34,
    TestTextAttr: 'Test text 1',
    TestTimestampAttr: new Date(),
    "ObjectReferenceAttr1": {
        "id": 100,
        "description": "Beschreibung1"
    },
    "ObjectReferenceAttr2": {
        "id": 555,
        "description": "Beschreibung2"
    }
}

storiesOf('JDynaMeta/Panels/Board GeneratedPanel2', module)
    .add('default', () => ({
        components: { 'jdy-generated-panel2': JdyGeneratedPanel2 },
        template: `<v-container class="grey lighten-5">
                    <jdy-generated-panel2 :classinfo="createdClass" :item-to-edit="editItem" :col-count="4">
                    </jdy-generated-panel2>
                  </v-container>`,
        data: () => ({ createdClass, editItem }),
    }));