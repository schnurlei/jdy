import { storiesOf } from '@storybook/vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../i18n";
import JdySimpleTable from "./JdySimpleTable.vue";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
export const textAttr = createdClass.addTextAttr('TestTextAttr', 30, null)
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);

const messages = { 'TestRepo':
                    {'TestClass':
                            {
                                'TestBooleanAttr': "TestBooleanEn",
                                'TestDecimalAttr': "TestDecimalAttrEn",
                                'TestTextAttr': "TestTextAttrEn",
                                'TestTimestampAttr': "TestTimestampAttrEn"
                            }

                    }
                };
i18n.mergeLocaleMessage('en', messages);
const allItems: any[] = [];

for (let i =0; i <= 12; i++) {
    allItems.push({ TestBooleanAttr: i%3,
        TestDecimalAttr: 22.34 + i,
        TestTextAttr: 'Test text ' +i,
        TestTimestampAttr: new Date(),
    });
}

const allColumns: any[]  = [
    { text: textAttr.getInternalName(), align: 'left', value: textAttr.getInternalName(), left: true},
    { text: booleanAttr.getInternalName(), align: 'left', value: booleanAttr.getInternalName(), left: true},
    { text: numAttr.getInternalName(), align: 'left', value: numAttr.getInternalName(), left: true},
    { text: timestampAttr.getInternalName(), align: 'left', value: timestampAttr.getInternalName(), left: true},
];


storiesOf('JDynaMeta/Panels/Board SimpleTable', module)
    .add('default', () => ({
        components: { 'jdy-simple-table': JdySimpleTable },
        template: `<v-container class="grey lighten-5">
                    <jdy-simple-table :classinfo="createdClass" :items="allItems" :columns="allColumns">
                    </jdy-simple-table>
                  </v-container>`,
        data: () => ({ createdClass, allItems, allColumns }),
    }))
    .add('with actions', () => ({
        components: { 'jdy-simple-table': JdySimpleTable },
        template: `<v-container class="grey lighten-5">
        <jdy-simple-table :classinfo="createdClass" :items="allItems" :columns="allColumns">
          <template v-slot:item-action="{ item }">
            <v-icon small>
              mdi-pencil
            </v-icon>
            <v-icon small>
              mdi-delete
            </v-icon>
          </template>
          <template v-slot:header-action>
            <v-btn color="primary" dark class="mb-2">New Item</v-btn>
          </template>
        </jdy-simple-table>
        </v-container>`,
        data: () => ({ createdClass, allItems, allColumns }),
    }));