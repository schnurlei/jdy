import { storiesOf } from '@storybook/vue';
import JdyObjectReferenceField from './JdyObjectReferenceField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
const referenzClass = rep.addClassInfo('ReferenzClass', null);
referenzClass.addLongAttr("id", 0 , 100000).setIsKey(true);
referenzClass.addTextAttr("description", 2000 , null);
export const refAttr1 = createdClass.addReference('ObjectReferenceAttr1', referenzClass);

const messages = { 'TestRepo':
                        {'TestClass':
                                {'TestBooleanAttr': "TestBooleanEn",
                                 'ObjectReferenceAttr1': "ObjectReferenceAttr1En"}
                        }
};
i18n.mergeLocaleMessage('en', messages);

const editItem = {
    TestBooleanAttr: true,
    "ObjectReferenceAttr1": {
        "id": 100,
        "description": "Beschreibung1"
    }
}

storiesOf('JDynaMeta/Components/Board JdyObjectReferenceField', module)
    .add('default', () => ({
        components: { 'jdy-object-reference': JdyObjectReferenceField },
        template: `<v-container class="grey lighten-5">
                    <v-row>
                      <v-col><jdy-object-reference :ref-attr="refAttr1" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                  </v-container>`,
        data: () => ({ refAttr1, editItem }),
    }));