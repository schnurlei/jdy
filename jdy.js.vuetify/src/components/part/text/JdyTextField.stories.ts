import { storiesOf } from '@storybook/vue';
import JdyTextField from './JdyTextField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const textAttr = createdClass.addVarCharAttr('TestVarCharAttr', 30)
textAttr.setNotNull(true);

const messages = { 'TestRepo':
                            {'TestClass':
                                    {'TestVarCharAttr': 'TestVarCharAttrEn'}
                        }};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestVarCharAttr: 'rsc'}

storiesOf('JDynaMeta/Components/Board JdyTextField', module)
    .add('default', () => ({
        components: { 'jdy-text': JdyTextField },
        template: `<v-container class="grey lighten-5">
                      <v-row>
                        <v-col><jdy-text :prim-attr="textAttr" :item-to-edit="editItem"/>
                        </v-col>
                      </v-row>
                   </v-container>`,
        data: () => ({ textAttr, editItem }),
    }));