import { storiesOf } from '@storybook/vue';
import JdyTimestampField from './JdyTimestampField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);

const messages = { 'TestRepo': {'TestClass': {'TestTimestampAttr': "TestTimestampEn"}}};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestTimestampAttr: new Date()}

storiesOf('JDynaMeta/Components/Board JdyTimestampField', module)
    .add('default', () => ({
        components: { 'jdy-timestamp': JdyTimestampField },
        template: `<v-container class="grey lighten-5">
                    <v-row>
                      <v-col><jdy-timestamp :prim-attr="timestampAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                  </v-container>`,
        data: () => ({ timestampAttr, editItem }),
    }));