import { storiesOf } from '@storybook/vue';
import JdyNumericTextField from './JdyNumericTextField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";

const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const numAttr = createdClass.addDecimalAttr('TestNumAttr', 20, 20, 2)

const editItem = { TestNumAttr: 30}
const labelText = "NumericLabel"

storiesOf('JDynaMeta/Components/Board JdyNumericTextField', module)
    .add('default', () => ({
        components: { 'jdy-numeric': JdyNumericTextField },
        template: `<v-container class="grey lighten-5">
                    <v-row>
                      <v-col>
                        <jdy-numeric :itemToEdit="editItem" :primAttr="numAttr" 
                                     :fieldLabel='labelText' :fieldReadonly="false" :disabled="false" 
                                     :isNotNull="true"
                                     :minValue="20" :maxValue="60" :scale="2">
                          
                        </jdy-numeric>
                      </v-col>
                    </v-row>
                   </v-container>`,
        data: () => ({ numAttr, editItem, labelText }),
    }));