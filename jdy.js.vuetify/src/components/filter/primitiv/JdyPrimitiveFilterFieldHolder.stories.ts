import { storiesOf } from '@storybook/vue';
import JdyPrimitiveFilterFieldHolder from './JdyPrimitiveFilterFieldHolder.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
export const textAttr = createdClass.addTextAttr('TestTextAttr', 30, null)
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);

const messages = { 'TestRepo':
                    {'TestClass':
                            {
                                'TestBooleanAttr': "TestBooleanEn",
                                'TestDecimalAttr': "TestDecimalAttrEn",
                                'TestTextAttr': "TestTextAttrEn",
                                'TestTimestampAttr': "TestTimestampAttrEn"
                            }

                    }
                };
i18n.mergeLocaleMessage('en', messages);

const editItem = {
    TestBooleanAttr: true,
    TestDecimalAttr: 22.34,
    TestTextAttr: 'Test text 1',
    TestTimestampAttr: new Date(),
    }

storiesOf('JDynaMeta/Filters/Board JdyPrimitiveFilterFieldHolder', module)
    .add('default', () => ({
        components: { 'jdy-filter-field': JdyPrimitiveFilterFieldHolder },
        template: `<v-container class="grey lighten-5">
                    <v-row>
                      <v-col><jdy-filter-field :prim-attr="booleanAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                    <v-row>
                      <v-col><jdy-filter-field :prim-attr="numAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                    <v-row>
                      <v-col><jdy-filter-field :prim-attr="textAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                    <v-row>
                      <v-col><jdy-filter-field :prim-attr="timestampAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                  </v-container>`,
        data: () => ({ booleanAttr, numAttr, textAttr, timestampAttr, editItem }),
    }));