import { storiesOf } from '@storybook/vue';
import JdyFilterPanel from './JdyFilterPanel.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {OperatorExprHolder} from "@jdynameta/jdy-view"
import {i18n} from "../../../i18n";

const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
export const textAttr = createdClass.addTextAttr('TestTextAttr', 30, null)
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);

const messages = { 'TestRepo':
                    {'TestClass':
                            {
                                'TestBooleanAttr': "TestBooleanEn",
                                'TestDecimalAttr': "TestDecimalAttrEn",
                                'TestTextAttr': "TestTextAttrEn",
                                'TestTimestampAttr': "TestTimestampAttrEn"
                            }

                    },
                    JdyFilterPanel: {
                        Attribute: 'Attribute',
                        Operator: 'Operator',
                        Value: 'Value',
                        Aktion: 'Action',
                        AddExpression: 'Add Expression',
                        SelectAttribute: 'Select Attribut',
                        SelectOperator: 'Select Operator',
                        Save: 'Save',
                        Cancel: 'Cancel',
                        NoData: 'No expression defined'
                    }
                };
i18n.mergeLocaleMessage('en', messages);

const allExpressions: OperatorExprHolder[] = []

storiesOf('JDynaMeta/Panels/Board JdyFilterPanel', module)
    .add('default', () => ({
        components: { 'jdy-filter-panel': JdyFilterPanel },
        template: `<v-container class="grey lighten-5">
                    <jdy-filter-panel :classinfo="createdClass" :filter-expressions="allExpressions">
                    </jdy-filter-panel>
                  </v-container>`,
        data: () => ({ createdClass, allExpressions }),
    }));