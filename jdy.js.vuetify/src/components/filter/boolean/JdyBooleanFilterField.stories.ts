import { storiesOf } from '@storybook/vue';
import JdyBooleanFilterField from './JdyBooleanFilterField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');

const messages = { 'TestRepo': {'TestClass': {'TestBooleanAttr': "TestBooleanEn"}}};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestBooleanAttr: true}

storiesOf('JDynaMeta/Filters/Board JdyBooleanField', module)
    .add('default', () => ({
        components: { 'jdy-boolean-filter': JdyBooleanFilterField },
        template: `<v-container class="grey lighten-5">
                    <v-row>
                      <v-col><jdy-boolean-filter :prim-attr="booleanAttr" :item-to-edit="editItem"/>
                      </v-col>
                    </v-row>
                  </v-container>`,
        data: () => ({ booleanAttr, editItem }),
    }));