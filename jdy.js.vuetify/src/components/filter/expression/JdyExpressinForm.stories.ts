import { storiesOf } from '@storybook/vue';
import JdyExpressionForm from './JdyExpressionForm.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";
import {OperatorExprHolder} from "@jdynameta/jdy-view"


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const booleanAttr = createdClass.addBooleanAttr('TestBooleanAttr');
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
export const textAttr = createdClass.addTextAttr('TestTextAttr', 30, null)
export const timestampAttr = createdClass.addTimeStampAttr('TestTimestampAttr', true, false);

const messages = { 'TestRepo':
                    {'TestClass':
                            {
                                'TestBooleanAttr': "TestBooleanEn",
                                'TestDecimalAttr': "TestDecimalAttrEn",
                                'TestTextAttr': "TestTextAttrEn",
                                'TestTimestampAttr': "TestTimestampAttrEn"
                            }

                    }
                };
i18n.mergeLocaleMessage('en', messages);

const theExpressions: OperatorExprHolder = {operator: null, attribute: textAttr, value: null};

storiesOf('JDynaMeta/Filters/Board JdyExpressionForm', module)
    .add('default', () => ({
        components: { 'jdy-expr-form': JdyExpressionForm },
        template: `<v-container class="grey lighten-5">
                      <jdy-expr-form :classInfo="createdClass" :expression="allExpressions"/>
                  </v-container>`,
        data: () => ({ createdClass, allExpressions: theExpressions }),
    }));