import { storiesOf } from '@storybook/vue';
import JdyStringFilterField from './JdyStringFilterField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const textAttr = createdClass.addTextAttr('TestTestAttr', 30, null)
textAttr.setNotNull(true);

export const textAttrEnum = createdClass
    .addTextAttr('TestTestAttrEnum', 40, ["enumWert1","enumWert2","enumWert3"])

const messages = { 'TestRepo':
                            {'TestClass':
                                    {'TestTestAttr': 'TestStringEn',
                                     'TestTestAttrEnum': 'TestTestAttrEnumEn'}
                        }};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestTestAttr: 'rsc'}
const editItemEnum = { TestTestAttrEnum: 'enumWert2'}

storiesOf('JDynaMeta/Filters/Board JdyStringField', module)
    .add('default', () => ({
        components: { 'jdy-string-filter': JdyStringFilterField },
        template: `<v-container class="grey lighten-5"><v-row><v-col>
                      <jdy-string-filter :prim-attr="textAttr" :item-to-edit="editItem"/>
                    </v-col></v-row></v-container>`,
        data: () => ({ textAttr, editItem }),
    }))
    .add('enum', () => ({
        components: { 'jdy-string-filter': JdyStringFilterField },
        template: `<v-container class="grey lighten-5"><v-row><v-col>
                        <jdy-string-filter :prim-attr="textAttrEnum" :item-to-edit="editItemEnum"/>
                    </v-col></v-row></v-container>`,
        data: () => ({ textAttrEnum, editItemEnum }),
    }));