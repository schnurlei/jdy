import { storiesOf } from '@storybook/vue';
import JdyLongFilterField from './JdyLongFilterField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const numAttr = createdClass.addLongAttr('TestLongAttr', 20, 60)
numAttr.setNotNull(true);

export const numAttrEnum = createdClass
    .addLongAttr('TestTestAttrEnum', 0, 100, [2,4,6])


const messages = { 'TestRepo':
                            {'TestClass':
                                    {'TestLongAttr': 'TestLongAttrEn',
                                        'TestTestAttrEnum': 'TestTestAttrEnumEn'}
                        }};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestLongAttr: 4}
const editItemEnum = { TestTestAttrEnum: 4}

storiesOf('JDynaMeta/Filters/Board JdyLongFilterField', module)
    .add('default', () => ({
        components: { 'jdy-long-filter': JdyLongFilterField },
        template: `<v-container class="grey lighten-5">
                      <v-row>
                        <v-col><jdy-long-filter :prim-attr="numAttr" :item-to-edit="editItem"/>
                        </v-col>
                      </v-row>
                   </v-container>`,
        data: () => ({ numAttr, editItem }),
    })) .add('enum', () => ({
        components: { 'jdy-long-filter': JdyLongFilterField },
        template: `<v-container class="grey lighten-5">
        <v-row>
          <v-col><jdy-long-filter :prim-attr="numAttrEnum" :item-to-edit="editItemEnum"/>
          </v-col>
        </v-row>
        </v-container>`,
        data: () => ({ numAttrEnum, editItemEnum }),
}));