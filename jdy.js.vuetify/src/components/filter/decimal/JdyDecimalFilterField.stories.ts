import { storiesOf } from '@storybook/vue';
import JdyDecimalFilterField from './JdyDecimalFilterField.vue';
import {JdyRepository} from "@jdynameta/jdy-base";
import {i18n} from "../../../i18n";


const rep = new JdyRepository('TestRepo');
const createdClass = rep.addClassInfo('TestClass', null);
export const numAttr = createdClass.addDecimalAttr('TestDecimalAttr', 20, 60, 2)
numAttr.setNotNull(true);

export const numAttrEnum = createdClass
    .addDecimalAttr('TestTestAttrEnum', 0, 100, 1,[1.1,1.2,1.3])


const messages = { 'TestRepo':
                            {'TestClass':
                                    {'TestDecimalAttr': 'TestDecimalAttrEn',
                                     'TestTestAttrEnum': 'TestTestAttrEnumEn'}
                        }};
i18n.mergeLocaleMessage('en', messages);

const editItem = { TestDecimalAttr: 10.12}
const editItemEnum = { TestTestAttrEnum: 1.2}

storiesOf('JDynaMeta/Filters/Board JdyDecimalFilterField', module)
    .add('default', () => ({
        components: { 'jdy-decimal-filter': JdyDecimalFilterField },
        template: `<v-container class="grey lighten-5">
                      <v-row>
                        <v-col><jdy-decimal-filter :prim-attr="numAttr" :item-to-edit="editItem"/>
                        </v-col>
                      </v-row>
                   </v-container>`,
        data: () => ({ numAttr, editItem })
    }))
    .add('enum', () => ({
        components: {'jdy-decimal-filter': JdyDecimalFilterField},
        template: `
          <v-container class="grey lighten-5">
          <v-row>
            <v-col>
              <jdy-decimal-filter :prim-attr="numAttrEnum" :item-to-edit="editItemEnum"/>
            </v-col>
          </v-row>
          </v-container>`,
        data: () => ({numAttrEnum, editItemEnum})
    }));