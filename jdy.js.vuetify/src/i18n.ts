import VueI18n from 'vue-i18n';
import Vue from 'vue';

Vue.use(VueI18n);

const messages = {
    en: {
        TestRepo: {
            TestClass: {
                TestTextAttr: 'Items per page:',
            }
        },
        JdyFilterPanel: {
            Attribute: 'Attribute',
        }
    }
}

export const i18n = new VueI18n({
    locale: 'en',
    messages // set locale
});
